#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# makes test of ABLORcli.py

import os
import filecmp


def cleanup():
    # files to clean up:
    toremove = ['orders/O1-W1.xml',
                'wantedlists/W1+O1.xml',
                'otherdirectory/A.xml',
                'otherdirectory/B.xml',
                'orders/O1-W1-W1-W1-W1-W1.xml',
                'orders/O1-W1-W1-W1-W1.xml',
                'orders/O1-W1-W1-W1.xml',
                'orders/O1-W1-W1.xml',
                'orders/O1-W1.xml',
                'wantedlists/W1+O1-W1-W1-W1-W1.xml',
                'wantedlists/W1+O1-W1-W1-W1.xml',
                'wantedlists/W1+O1-W1-W1.xml',
                'wantedlists/W1+O1-W1.xml',
                'wantedlists/W1+O1.xml']
    for f in toremove:
        if os.path.exists(f):
            os.remove(f)

tid = 0

# --- test 1 show info
tid += 1
ts = 'Test type: cmd line show info'
cleanup()
rv = os.system('python3 ../ABLORcli.py')
if not rv:
    raise Exception('test ' + str(tid) + ' return value incorrect!\n' + ts)

# --- test 2 too few input arguments
tid += 1
ts = 'Test type: cmd line too few input arguments'
cleanup()
rv = os.system('python3 ../ABLORcli.py a')
if not rv:
    raise Exception('test ' + str(tid) + ' return value incorrect!\n' + ts)

# --- test 3 too many input arguments
tid += 1
ts = 'Test type: cmd line too many input arguments'
cleanup()
rv = os.system('python3 ../ABLORcli.py a b c d e')
if not rv:
    raise Exception('test ' + str(tid) + ' return value incorrect!\n' + ts)

# --- test 4 real input arguments
tid += 1
ts = 'Test type: cmd line 2 real input arguments'
cleanup()
rv = os.system('python3 ../ABLORcli.py orders/O1.xml wantedlists/W1.xml')
if rv:
    raise Exception('test ' + str(tid) + ' return value incorrect!\n' + ts)
if not os.path.exists('orders/O1-W1.xml'):
    raise Exception('test ' + str(tid) + ' output file missing!\n' + ts)
if not os.path.exists('wantedlists/W1+O1.xml'):
    raise Exception('test ' + str(tid) + ' output file missing!\n' + ts)
if not filecmp.cmp('orders/O1-W1.xml', 'correctresults/O1-W1.xml', False):
    raise Exception('test ' + str(tid) + ' output file incorrect!\n' + ts)
if not filecmp.cmp('wantedlists/W1+O1.xml', 'correctresults/W1+O1.xml', False):
    raise Exception('test ' + str(tid) + ' output file incorrect!\n' + ts)

# --- test 5 real input arguments
tid += 1
ts = 'Test type: cmd line 4 real input arguments '
cleanup()
rv = os.system('python3 ../ABLORcli.py orders/O1.xml wantedlists/W1.xml otherdirectory/A.xml otherdirectory/B.xml')
if rv:
    raise Exception('test ' + str(tid) + ' return value incorrect!\n' + ts)
if not os.path.exists('otherdirectory/A.xml'):
    raise Exception('test ' + str(tid) + ' output file missing!\n' + ts)
if not os.path.exists('otherdirectory/B.xml'):
    raise Exception('test ' + str(tid) + ' output file missing!\n' + ts)
if not filecmp.cmp('otherdirectory/A.xml', 'correctresults/O1-W1.xml', False):
    raise Exception('test ' + str(tid) + ' output file incorrect!\n' + ts)
if not filecmp.cmp('otherdirectory/B.xml', 'correctresults/W1+O1.xml', False):
    raise Exception('test ' + str(tid) + ' output file incorrect!\n' + ts)

# --- test 6 repeated application of order onto new and new wanted list to check
# depletion of order to emptiness
tid += 1
ts = 'Test type: cmd line repeated application of order onto WL'
cleanup()
rv = os.system('python3 ../ABLORcli.py orders/O1.xml wantedlists/W1.xml')
if rv:
    raise Exception('test ' + str(tid) + ' return value incorrect!\n' + ts)
rv = os.system('python3 ../ABLORcli.py orders/O1-W1.xml wantedlists/W1.xml')
if rv:
    raise Exception('test ' + str(tid) + ' return value incorrect!\n' + ts)
rv = os.system('python3 ../ABLORcli.py orders/O1-W1-W1.xml wantedlists/W1.xml')
if rv:
    raise Exception('test ' + str(tid) + ' return value incorrect!\n' + ts)
rv = os.system('python3 ../ABLORcli.py orders/O1-W1-W1-W1.xml wantedlists/W1.xml')
if rv:
    raise Exception('test ' + str(tid) + ' return value incorrect!\n' + ts)
rv = os.system('python3 ../ABLORcli.py orders/O1-W1-W1-W1-W1.xml wantedlists/W1.xml')
if rv:
    raise Exception('test ' + str(tid) + ' return value incorrect!\n' + ts)
if not filecmp.cmp('orders/O1-W1-W1-W1-W1-W1.xml', 'correctresults/O1-W1-W1-W1-W1-W1.xml', False):
    raise Exception('test ' + str(tid) + ' output file incorrect!\n' + ts)

# --- finish:
print('ALL OK!')
