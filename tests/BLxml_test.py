#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# tests BLxml, ablor, mablor, others
# 2DO mablor

# XXX
#  vyzkouset-vadny-O14690972.xml
#  In [53]: W=BLxml.BLlist(f)
#    File "<string>", line unknown
#  ParseError: mismatched tag: line 6, column 18
#

import BLxml
import filecmp
import os
from shutil import copyfile

# CONSTS:
# directories:
Odir = 'tests/orders/'
Wdir = 'tests/wantedlists/'
Cdir = 'tests/correctresults/'
Tdir = 'tests/tmp/'
# files:
W1fn = 'W1'
W2fn = 'W2'
W3fn = 'W3'
O1fn = 'O1'
# cli command:
CLI = 'python3 BLxmlcli.py '


def cleanup() -> None:
    # remove all files from Tdir
    files = [f for f in os.listdir(Tdir)
             if os.path.isfile(os.path.join(Tdir, f))]
    for f in files:
        os.remove(Tdir + f)


def test_load_list(tid):
    # test loading list
    BLxml.BLlist(Wdir + W1fn + '.xml')


def test_save_list(tid):
    # test saving xml
    ts = 'Test type: saving list as xml'
    W1 = BLxml.BLlist(Wdir + W1fn + '.xml')
    W1.xml_save(Tdir + W1fn + '_save.xml')
    # check saved file:
    rv = filecmp.cmp(Cdir + W1fn + '_save.xml',
                     Tdir + W1fn + '_save.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)


def test_save_html(tid):
    # test saving html
    ts = 'Test type: saving list as html'
    W1 = BLxml.BLlist(Wdir + W1fn + '.xml')
    W1.name = W1fn + '_save'
    W1.html_save(Tdir + W1fn + '_save.html')
    # compare whole file
    rv = filecmp.cmp(Cdir + W1fn + '_save.html',
                     Tdir + W1fn + '_save.html',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)

    # testing cli - delete and create file(s) again
    # test input as actual file:
    os.remove(Tdir + W1fn + '_save.html')
    cmd = CLI + ' html ' + Tdir + W1fn + '_save.xml'
    r = os.system(cmd)
    if r:
        raise Exception('test ' + str(tid) + ', cli, failed!\n' +
                        ts + '\n' + cmd)
    rv = filecmp.cmp(Cdir + W1fn + '_save.html',
                     Tdir + W1fn + '_save.xml.html',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    # test input as directory:
    os.remove(Tdir + W1fn + '_save.xml.html')
    cmd = CLI + ' html ' + Tdir
    r = os.system(cmd)
    if r:
        raise Exception('test ' + str(tid) + ', cli, failed!\n' +
                        ts + '\n' + cmd)
    rv = filecmp.cmp(Cdir + W1fn + '_save.html',
                     Tdir + W1fn + '_save.xml.html',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)


def test_create_item(tid):
    # test creating new item in list
    ts = 'Test type: creating new item in list'
    W1 = BLxml.BLlist(Wdir + W1fn + '.xml')
    W3 = None
    W3 = W1.copy()
    cornerbrick = W3.create_item('P', '2357')
    cornerbrick.color = 1
    cornerbrick.maxprice = 10
    cornerbrick.minqty = 20
    cornerbrick.qtyfilled = 30
    cornerbrick.condition = 'N'
    cornerbrick.remarks = 'remark'
    cornerbrick.notify = 'Y'
    cornerbrick.wantedshow = '40'
    cornerbrick.wantedid = 50
    # compare whole file
    W3.xml_save(Tdir + W3fn + '_createitem.xml')
    rv = filecmp.cmp(Cdir + W3fn + '_createitem.xml',
                     Tdir + W3fn + '_createitem.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)


def test_remove_item(tid):
    # test removing item from list
    ts = 'Test type: removing item from list'
    W1 = BLxml.BLlist(Wdir + W1fn + '.xml')
    W3 = W1.copy()
    items = W3.all_items()
    W3.rem_item(items[0])
    W3.rem_item(items[2])
    W3.rem_item(items[4])
    # compare whole file
    W3.xml_save(Tdir + W3fn + '_removed.xml')
    rv = filecmp.cmp(Cdir + W3fn + '_removed.xml',
                     Tdir + W3fn + '_removed.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)


def test_add_lists(tid):
    # test adding two lists
    ts = 'Test type: adding two lists'
    W1 = BLxml.BLlist(Wdir + W1fn + '.xml')
    W2 = BLxml.BLlist(Wdir + W2fn + '.xml')
    W3 = None
    W3 = W1 + W2
    # compare whole file
    W3.xml_save(Tdir + W3fn + '_added.xml')
    rv = filecmp.cmp(Cdir + W3fn + '_added.xml',
                     Tdir + W3fn + '_added.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)

    # testing cli - delete and create file(s) again
    # test input as actual files
    cleanup()
    cmd = CLI + ' add ' + \
        Wdir + W1fn + '.xml' + ' ' + \
        Wdir + W2fn + '.xml' + ' ' + \
        Tdir + W3fn + '_added.xml'
    r = os.system(cmd)
    if r:
        raise Exception('test ' + str(tid) + ', cli, failed!\n' +
                        ts + '\n' + cmd)
    rv = filecmp.cmp(Cdir + W3fn + '_added.xml',
                     Tdir + W3fn + '_added.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    # test input as directory
    cleanup()
    copyfile(Wdir + W1fn + '.xml', Tdir + W1fn + '.xml')
    copyfile(Wdir + W2fn + '.xml', Tdir + W2fn + '.xml')
    cmd = CLI + ' add ' + \
        Tdir + ' ' + \
        Tdir + W3fn + '_added.xml'
    r = os.system(cmd)
    if r:
        raise Exception('test ' + str(tid) + ', cli, failed!\n' +
                        ts + '\n' + cmd)
    rv = filecmp.cmp(Cdir + W3fn + '_added.xml',
                     Tdir + W3fn + '_added.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)


def test_check_status(tid):
    # test check status
    ts = 'Test type: list status'
    W1 = BLxml.BLlist(Wdir + W1fn + '.xml')
    [have_bricks, bricks, filled_lots, lots] = W1.status()
    if not (have_bricks == 39 and
            bricks == 99 and
            filled_lots == 1 and
            lots == 12):
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)


def test_ablor(tid):
    # test check ABLOR
    ts = 'Test type: ABLOR'
    W1 = BLxml.BLlist(Wdir + W1fn + '.xml')
    O1 = BLxml.BLlist(Odir + O1fn + '.xml')
    BLxml.ABLOR(O1, W1)
    W1.xml_save(Tdir + W1fn + '_ablor.xml')
    W1.html_save(Tdir + W1fn + '_ablor.html')
    O1.xml_save(Tdir + O1fn + '_ablor.xml')
    O1.html_save(Tdir + O1fn + '_ablor.html')
    rv = filecmp.cmp(Cdir + W1fn + '_ablor.xml',
                     Tdir + W1fn + '_ablor.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    rv = filecmp.cmp(Cdir + O1fn + '_ablor.xml',
                     Tdir + O1fn + '_ablor.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    rv = filecmp.cmp(Cdir + W1fn + '_ablor.html',
                     Tdir + W1fn + '_ablor.html',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    rv = filecmp.cmp(Cdir + O1fn + '_ablor.html',
                     Tdir + O1fn + '_ablor.html',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    # testing cli - delete and create file(s) again
    # test input as actual files
    cleanup()
    cmd = CLI + ' ablor ' + \
        Odir + O1fn + '.xml' + ' ' + \
        Wdir + W1fn + '.xml' + ' ' + \
        Tdir + O1fn + '_ablor.xml' + ' ' + \
        Tdir + W1fn + '_ablor.xml'
    r = os.system(cmd)
    if r:
        raise Exception('test ' + str(tid) + ', cli, failed!\n' +
                        ts + '\n' + cmd)
    rv = filecmp.cmp(Cdir + W1fn + '_ablor.xml',
                     Tdir + W1fn + '_ablor.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    rv = filecmp.cmp(Cdir + O1fn + '_ablor.xml',
                     Tdir + O1fn + '_ablor.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    # test input as actual files with set suffix
    cleanup()
    copyfile(Wdir + W1fn + '.xml', Tdir + W1fn + '.xml')
    copyfile(Odir + O1fn + '.xml', Tdir + O1fn + '.xml')
    cmd = CLI + ' ablor ' + \
        Tdir + O1fn + '.xml' + ' ' + \
        Tdir + W1fn + '.xml' + ' ' + \
        '--suffix _ablor'
    r = os.system(cmd)
    if r:
        raise Exception('test ' + str(tid) + ', cli, failed!\n' +
                        ts + '\n' + cmd)
    rv = filecmp.cmp(Cdir + W1fn + '_ablor.xml',
                     Tdir + W1fn + '.xml_ablor.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    rv = filecmp.cmp(Cdir + O1fn + '_ablor.xml',
                     Tdir + O1fn + '.xml_ablor.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    # test clean
    cleanup()
    cmd = CLI + ' ablor ' + \
        ' --clean ' + \
        Odir + O1fn + '.xml' + ' ' + \
        Wdir + W1fn + '.xml' + ' ' + \
        Tdir + O1fn + '_ablor_clean.xml' + ' ' + \
        Tdir + W1fn + '_ablor_clean.xml'
    r = os.system(cmd)
    if r:
        raise Exception('test ' + str(tid) + ', cli, failed!\n' +
                        ts + '\n' + cmd)
    rv = filecmp.cmp(Cdir + W1fn + '_ablor_clean.xml',
                     Tdir + W1fn + '_ablor_clean.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    rv = filecmp.cmp(Cdir + O1fn + '_ablor_clean.xml',
                     Tdir + O1fn + '_ablor_clean.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    # test html creation
    cleanup()
    cmd = CLI + ' ablor ' + \
        ' --html ' + \
        Odir + O1fn + '.xml' + ' ' + \
        Wdir + W1fn + '.xml' + ' ' + \
        Tdir + O1fn + '_ablor.xml' + ' ' + \
        Tdir + W1fn + '_ablor.xml'
    r = os.system(cmd)
    if r:
        raise Exception('test ' + str(tid) + ', cli, failed!\n' +
                        ts + '\n' + cmd)
    rv = filecmp.cmp(Cdir + W1fn + '_ablor.xml',
                     Tdir + W1fn + '_ablor.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    rv = filecmp.cmp(Cdir + O1fn + '_ablor.xml',
                     Tdir + O1fn + '_ablor.xml',
                     shallow=False)
    if not rv:
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    if not os.path.exists(Tdir + W1fn + '_ablor.xml.html'):
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)
    if not os.path.exists(Tdir + O1fn + '_ablor.xml.html'):
        raise Exception('test ' + str(tid) + ' failed!\n' + ts)


# initialization of the test script:
cleanup()
tid = 1
test_load_list(tid)
tid += 1
test_save_list(tid)
tid += 1
test_save_html(tid)  # tests also cli
tid += 1
test_create_item(tid)
tid += 1
test_remove_item(tid)
tid += 1
test_add_lists(tid)  # tests also cli
tid += 1
test_check_status(tid)
tid += 1
test_ablor(tid)  # tests also cli

# --- finish:
print('ALL OK! (' + str(tid) + ' tests)')
