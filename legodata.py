#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# class for parsing xml data from bricklink
# converts color id or part id into color name,hex or part name
# data were obtained by downloading from bricklink.
# how to donwload xml data files:
# go to bricklink, login, catalogue->download

from os import path as pth
# xml processing:
import xml.etree.ElementTree as ET


class catalogue():

    def __init__(self, resourcedir):
        # input: resourcedir is a directory with
        # files colors.xml and Parts.xml
        self.colors = ET.parse(pth.join(resourcedir, 'colors.xml'))
        self.parts = ET.parse(pth.join(resourcedir, 'Parts.xml'))

    def color_name(self, colorid):
        # input: str/int with brick color identification number
        # output: string with color name based on colorid value
        if colorid == 'any':
            return ''
        colorid = str(colorid)
        for item in self.colors.iter('ITEM'):
            if colorid == item.find('COLOR').text:
                return item.find('COLORNAME').text
        return 'Unknown color name'

    def color_hex(self, colorid):
        if colorid == 'any':
            return ''
        # input: str/int with brick color identification number
        # output: string with color hexadecimal value based on colorid value
        colorid = str(colorid)
        for item in self.colors.iter('ITEM'):
            if colorid == item.find('COLOR').text:
                return item.find('COLORRGB').text
        return 'Unknown color'

    def part_name(self, partid):
        # input: str/int with brick part identification number
        # output: string with brink name based on partid value
        partid = str(partid)
        for item in self.parts.iter('ITEM'):
            if partid == item.find('ITEMID').text:
                return item.find('ITEMNAME').text
        return 'Unknown part name'
