#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# 2DO:
# rename all to: html REPORT
# result files
# BL list
#
# possible input arguments:
# BLxmlcli.py [-h] [-e] {html,add,sub,ablor,mablor} ...
#
# html   [-h] List [List]
# add    [-h] [--clean] [--html] List [List] output_list
# sub    [-h] [--clean] [--html] A_list B_list output_list
# ablor  [-h] [--clean] [--html] [-suffix SUFFIX]
#               O_list W_list [O_list_res] [W_list_res]
# mablor [-h] [--clean] [--html] [-suffix SUFFIX] [--summarize]
#               [-o O [O ...]] [-w W [W ...]]

import argparse
from os import path
import sys
import glob
import BLxml


def parse_path_argument(arglist):
    """Checks paths in arglist and make a list of .xml files."""
    xmlfiles = []
    for p in arglist:
        # check existence of path
        if not path.exists(p):
            print("Error: no file nor directory " +
                  "was found on path '" + p + "'.")
            sys.exit(1)
        # if file add to list, else load lists from directory
        if path.isfile(p):
            # it is file and exists, add it to list:
            xmlfiles.append(p)
        elif path.isdir(p):
            # it is directory, scan for files and add to list:
            xmlfiles.extend(glob.glob(path.join(p, '*.xml')))
        else:
            print("Error: no file nor directory " +
                  "was found on path '" + p + "'.")
            sys.exit(1)
    if len(xmlfiles) == 0:
        print("Error: no valid files nor valid files in directories " +
              "were specified in following paths:")
        print(arglist)
        sys.exit(1)
    return xmlfiles


def command_html(args):
    """Handles html mode of operation."""
    # check files/directory exist
    xmlfiles = parse_path_argument(args.L)
    # load BL lists, save as html:
    for f in xmlfiles:
        L = BLxml.BLlist(f)
        L.html_save(f + '.html')
    print(str(len(xmlfiles)) + ' files created.')


def command_add(args):
    """Handles add mode of operation."""
    # check files/directory exist
    xmlfiles = parse_path_argument(args.L)
    # load BL lists
    BLlists = []
    for f in xmlfiles:
        BLlists.append(BLxml.BLlist(f))
    # add all lists into one
    SL = BLlists[0].copy()
    SL.name = args.R[0]
    for i in range(1, len(BLlists)):
        SL = SL + BLlists[i]
    # make clean if needed
    if args.clean:
        SL.rem_full()
    # save summary list
    SL.xml_save(args.R[0])
    # make html if needed
    if args.html:
        SL.html_save(SL.name + '.html')


def command_sub(args):
    """Handles sub mode of operation."""
    # check files exist
    if not path.exists(args.A[0]):
        print("Error: no file was found on path '" + args.A[0] + "'.")
        sys.exit(1)
    if not path.exists(args.B[0]):
        print("Error: no file was found on path '" + args.B[0] + "'.")
        sys.exit(1)
    # load BL lists
    A_list = BLxml.BLlist(args.A[0])
    B_list = BLxml.BLlist(args.B[0])
    # subtract lists:
    # do clean, do html
    print(A)
    print(B)
    # XXX FINISH ME
    raise Exception('FINISH ME')


def command_ablor(args):
    """Handles ablor mode of operation."""
    # check files exist
    if not path.exists(args.O[0]):
        print("Error: no file was found on path '" + args.O[0] + "'.")
        sys.exit(1)
    if not path.exists(args.W[0]):
        print("Error: no file was found on path '" + args.W[0] + "'.")
        sys.exit(1)
    # check suffix and make filenames of output files
    if args.suffix is not None:
        suffix = args.suffix
    else:
        suffix = '_res'
    if args.RW is None and args.RO is None:
        # both arguments missing
        fnWres = args.W[0] + suffix + '.xml'
        fnOres = args.O[0] + suffix + '.xml'
        fnWreshtml = args.W[0] + suffix + '.html'
        fnOreshtml = args.O[0] + suffix + '.html'
    elif args.W is not None and args.RO is not None:
        # both arguments specified
        fnWres = args.RW
        fnOres = args.RO
        fnWreshtml = args.RW + '.html'
        fnOreshtml = args.RO + '.html'
    else:
        # one of the arguments was set and the other not
        print("Either both or neither arguments " +
              "W_list_res and O_list_res must be specified.")
        sys.exit(1)
    # load BL lists
    O_list = BLxml.BLlist(args.O[0])
    W_list = BLxml.BLlist(args.W[0])
    # do ablor
    BLxml.ABLOR(O_list, W_list)
    # make clean if needed
    if args.clean:
        O_list.rem_full()
        W_list.rem_full()
    # save output lists
    O_list.xml_save(fnOres)
    W_list.xml_save(fnWres)
    # make html if needed
    if args.html:
        O_list.xml_save(fnOreshtml)
        W_list.xml_save(fnWreshtml)


def command_mablor(args):
    """Handles mablor mode of operation."""
    # check both arguments are present:
    if not args.O:
        print("Error: argument -o with BL order lists is missing.")
        sys.exit(1)
    if not args.W:
        print("Error: argument -w with BL wanted lists is missing.")
        sys.exit(1)
    # check files exist
    Oxmlfiles = parse_path_argument(args.O)
    Wxmlfiles = parse_path_argument(args.W)
    # check suffix and make filenames of output files
    if args.suffix is not None:
        suffix = args.suffix
    else:
        suffix = '_res'
    # load BL lists
    O_lists = []
    for L in Oxmlfiles:
        O_lists.append(BLxml.BLlist(L))
    W_lists = []
    for L in Wxmlfiles:
        W_lists.append(BLxml.BLlist(L))
    # do mablor
    BLxml.MABLOR(O_lists, W_lists)
    # make clean if needed
    if args.clean:
        for L in O_lists:
            L.rem_full()
        for L in W_lists:
            L.rem_full()
    # save output lists
    for L in O_lists:
        if args.clean:
            L.rem_full()
        L.xml_save(L.name + suffix + '.xml')
    for L in W_lists:
        if args.clean:
            L.rem_full()
        L.xml_save(L.name + suffix + '.xml')


# introductory help description:
descr = "\nBLxml is library for operations with Bricklink's XML files.\n" + \
        "Bricklink (BL) can export Wanted lists or Orders as xml files.\n" + \
        "The main function of this utility is in applying order list to wanted\n" + \
        "list. It means that lots with same bricks are found in both lists.\n" + \
        "The 'have quantity' in order is decreased and 'have quantity' in\n" + \
        "wanted list increased up to 'wanted quantity' of wanted list.\n" + \
        "Abilities:\n" + \
        "- creating html files for BL xml files with nice overview of the content,\n" + \
        "- addition of BL lists,\n" + \
        "- subtraction of BL lists,\n" + \
        "- applying order list(s) to wanted list(s).\n" + \
        "\n" + \
        "For details see:\n" + \
        "https://gitlab.com/KaeroDot/applyblorder/"
# examples for help:
ex = 'Examples:\n' + \
     '\n' + \
     '--1--\n' + \
     'show help for commands:\n' + \
     '    BLxml.py html -h\n' + \
     '    BLxml.py add -h\n' + \
     '    BLxml.py sub -h\n' + \
     '    BLxml.py ablor -h\n' + \
     '    BLxml.py mablor -h\n' + \
     '\n' + \
     '--2--\n' + \
     "Create html reports for BL lists 'list1', 'list2' and all xml files\n" + \
     "in the directory 'directory\with\lists':\n" + \
     '    BLxml.py html list1.xml list2.xml directory\with\lists\n' + \
     '\n' + \
     '--3--\n' + \
     'Add 3 BL lists together and save as result.xml:\n' + \
     '    BLxml.py add list1.xml list2.xml list3.xml result.xml\n' + \
     '\n' + \
     '--4--\n' + \
     "Add all BL lists (*.xml) in the directory 'directory\with\lists'\n" + \
     "together, clean full lots it and save as result.xml (full lots \n" + \
     "are the ones with 'have quantity' equal or larger than \n" + \
     "'wanted quantity'):\n" + \
     '    BLxml.py add directory\with\lists result.xml --clean\n' + \
     '\n' + \
     '--5--\n' + \
     "Subtract BL list 'list2.xml' from 'list1.xml' and save as result.xml:\n" + \
     '    BLxml.py sub list1.xml list2.xml result.xml\n' + \
     '\n' + \
     '--6--\n' + \
     "Apply BL order list in file 'order.xml' to BL wanted list\n" + \
     "in file 'wanted.xml' and save as new files:\n" + \
     '    BLxml.py ablor order.xml wanted.xml\n' + \
     'Files order_new.xml and wanted_new.xml will be created.\n' + \
     '\n' + \
     '--7--\n' + \
     'Apply BL order list to BL wanted list and specify\n' + \
     'names of result files:\n' + \
     '    BLxml.py ablor order.xml wanted.xml order_ablor.xml wanted_ablor.xml\n' + \
     '\n' + \
     '--8--\n' + \
     'Apply BL order list to BL wanted list and specify\n' + \
     'names of output files using suffix parameter:\n' + \
     '    BLxml.py ablor order.xml wanted.xml --suffix _ablor\n' + \
     'Files order_ablor.xml and wanted_ablor.xml will be created.\n' + \
     '\n' + \
     '--9--\n' + \
     "Apply BL order list to BL wanted list and remove all lots that are full\n" + \
     "from the output files.\n" + \
     "Argument --html will create html reports for output xml files.\n" + \
     '    BLxml.py ablor order.xml wanted.xml --clean --html --suffix _ablor\n' + \
     '\n' + \
     '--10--\n' + \
     'Apply 2 BL order lists to 2 BL wanted lists:\n' + \
     '    BLxml.py mablor -o order1.xml order2.xml -w wanted1.xml wanted2.xml\n' + \
     "Files 'order1_new.xml', 'order2_new.xml', 'wanted1_new.xml',\n" + \
     "wanted2_new.xml' will be created.\n" + \
     '\n' + \
     '--11--\n' + \
     "Apply BL order lists in directory 'orders' to BL wanted lists in\n" + \
     "directory 'wanted' and set suffix to '_M'. Output files will be\n" + \
     "cleaned and html reports will be generated:\n" + \
     '    BLxml.py mablor -o orders -w wanted --suffix _M --clean --html\n'

# create main parser
P = argparse.ArgumentParser(epilog=descr,
                            formatter_class=argparse.RawTextHelpFormatter)
P.add_argument('-e',
               action='store_true',
               help='Shows examples of use.')
SP = P.add_subparsers()

# subparser html
P_html = SP.add_parser('html',
                       help='Creates html from BL list(s).')
P_html.add_argument('L',
                    action='store',
                    nargs='+',
                    help='One or more xml files with BL lists or ' +
                         'directories with xml files with BL lists.')
P_html.set_defaults(func=command_html)

# subparser add
P_add = SP.add_parser('add',
                      help='Adds multiple BL lists L together and save as result R.')
P_add.add_argument('L',
                   action='store',
                   nargs='+',
                    help='One or more xml files with BL lists or ' +
                         'directories with xml files with BL lists.')
P_add.add_argument('R',
                   action='store',
                   nargs=1,
                   help='Filename of result file with sum of lists L.')
P_add.add_argument('--clean',
                   action='store_true',
                   help='Clean resulting output_list (remove full lots).')
P_add.add_argument('--html',
                   action='store_true',
                   help='Create html file for resulting output_list.')
P_add.set_defaults(func=command_add)

# subparser sub
P_sub = SP.add_parser('sub',
                      help='Subtracts two BL lists.')
P_sub.add_argument('A',
                   action='store',
                   nargs=1,
                   help='XML file with BL list A.')
P_sub.add_argument('B',
                   action='store',
                   nargs=1,
                   help='XML file with BL list B.')
P_sub.add_argument('R',
                   action='store',
                   nargs=1,
                   help='Filename of result file with subtraction A - B.')
P_sub.add_argument('--clean',
                   action='store_true',
                   help='Clean resulting output_list (remove full lots).')
P_sub.add_argument('--html',
                   action='store_true',
                   help='Create html file for resulting output_list.')
P_sub.set_defaults(func=command_sub)

# subparser ablor
P_ablor = SP.add_parser('ablor',
                        help='Applies BL order to BL wanted list.')
P_ablor.add_argument('O',
                     action='store',
                     nargs=1,
                     help='xml file with BL list containing an order.')
P_ablor.add_argument('W',
                     action='store',
                     nargs=1,
                     help='xml file with BL list containing a wanted list.')
P_ablor.add_argument('RO',
                     action='store',
                     nargs='?',
                     help='Filename for resulting order list. ' +
                          'When this argument is used, RW ' +
                          'must be also used.')
P_ablor.add_argument('RW',
                     action='store',
                     nargs='?',
                     help='Filename for resulting wanted list. ' +
                          'When this argument is used, ' +
                          'OR must be also used.')
P_ablor.add_argument('--clean',
                     action='store_true',
                     help='Clean resulting OR and WR (remove full lots).')
P_ablor.add_argument('--html',
                     action='store_true',
                     help='Create html file for resulting OR and WR.')
P_ablor.add_argument('--suffix',
                     action='store',
                     help='Suffix added to the resulting OR and WR ' + \
                          'files. If arguments OR and WR are ' +
                          'specified, this argument is overridden.')
P_ablor.set_defaults(func=command_ablor)

# subparser mablor
P_mablor = SP.add_parser('mablor',
                         help='Applies multiple BL orders ' +
                              'to multiple BL wanted lists.')
P_mablor.add_argument('--clean',
                      action='store_true',
                      help='Clean resulting output_lists (remove full lots).')
P_mablor.add_argument('--html',
                      action='store_true',
                      help='Create html file for resulting output_lists.')
P_mablor.add_argument('--suffix',
                      action='store',
                      help="Suffix added to the resulting output files." + \
                           "Default: '_new.'")
P_mablor.add_argument('--summarize',
                      action='store_true',
                      help='Generate summary lists for all orders ' +
                           'and all wanted lists.')
P_mablor.add_argument('-O',
                      action='store',
                      nargs='+',
                      help='XML file(s) with BL order ' +
                           'or directory with BL orders.')
P_mablor.add_argument('-W',
                      action='store',
                      nargs='+',
                      help='XML file(s) with BL wanted list ' +
                           'or directory with BL wanted lists.')
P_mablor.set_defaults(func=command_mablor)

# parse inputs and run functions
args = P.parse_args()
if args.e:
    print(ex)
else:
    args.func(args)
