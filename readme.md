# ABLOR

Software *ABLOR* applies *Bricklink order* to a *Bricklink wanted list*. Generates a
*new wanted list* with **increased have** count, and a *new order* with **decreased
ordered count**. Simplified scheme:

![ABLOR](img/scheme/results/ablor.png)

# MABLOR
Software *MABLOR* runs ABLOR multiple times. First applies all *orders* to a first *wanted list*,
than applies *orders with decreased brick count* to a second *wanted list* and so on.
The result is the same as using ABLOR manually several times.
Following animation shows 4 runs of ABLOR managed by MABLOR.

![MABLOR](img/scheme/results/mablor.gif)

# Properties
ABLOR and MABLOR can handle bricks with *any* (in bricklink *Not Applicable*) color. This means
first bricks in wanted list with specified color are supplied from an order. Next bricks with *any
(Not Applicable)* color are supplied by remaining bricks from the order.

# Motivation
Bricklink gives you possibility to have multiple wanted lists. Also you can
easily plan buying bricks from multiple wanted lists. Unfortunately, if you try
to apply your received order to first wanted list, and than to second wanted
list, you will find that you own more bricks than you order. Why? Because
applying order adds *have* bricks to the wanted list, but do not remove bricks
from the order.

# Download files
- *ABLORcli* and *MABLORcli* are console versions.
- *ABLOR* and *MABLOR* are version with graphical user interface.

Probably you want to download *MABLOR, Windows executable, GUI*.

- [**MABLOR, Linux executable, GUI**](https://gitlab.com/KaeroDot/applyblorder/-/raw/master/dist/MABLOR)
- [**MABLOR, Windows executable, GUI**](https://gitlab.com/KaeroDot/applyblorder/-/raw/master/dist/MABLOR.exe)

- [ABLOR, Linux executable, GUI](https://gitlab.com/KaeroDot/applyblorder/-/raw/master/dist/ABLOR)
- [ABLOR, Windows executable, GUI](https://gitlab.com/KaeroDot/applyblorder/-/raw/master/dist/ABLOR.exe)

- [MABLORcli, Linux executable, console](https://gitlab.com/KaeroDot/applyblorder/-/raw/master/dist/MABLORcli)
- [MABLORcli, Windows executable, console](https://gitlab.com/KaeroDot/applyblorder/-/raw/master/dist/MABLORcli.exe)

- [ABLORcli, Linux executable, console](https://gitlab.com/KaeroDot/applyblorder/-/raw/master/dist/ABLORcli)
- [ABLORcli, Windows executable, console](https://gitlab.com/KaeroDot/applyblorder/-/raw/master/dist/ABLORcli.exe)

# How to use MABLOR
1. Get all wanted lists from Bricklink as .xml files, see lower how to do it.

1. Get all orders from Bricklink as .xml files, see lower how to do it.

1. Run the software.

   ![](img/MABLOR.jpg)

1. Drag & drop wanted list files into left grid.

1. Drag & drop order files into second grid from left.

1. Click *Start* button.

1. Upload new Updated wanted lists back to Bricklink, see lower how to do it.

# How to use ABLOR
1. Run the software.

   ![](img/applyBLorderG.jpg)

1. Using buttons select a .xml file with wanted list, select .xml file with
   order, or write their names into text boxes, or drag and drop.

1. Output order file name and output wanted list file name is generated
   automatically (if not set by user).

1. Click on button *Apply*

1. You should get files named according scheme 'order minus wantedlist .xml' and
   'wantedlist plus order.xml'. Also log in html format is created: 'wantedlist plus order=LOG.html'

1. Review the new modified wanted list by opening html log in webbrowser.

   Notice the last line showing how wanted list items (bricks) with color *any* are treated. Such wanted
   list items are treated *last*. Any bricks left are used to fill this item. On row 4 of
   example (orange), you see *Left in order* is 2. These two last bricks from order were used to
   supply demand of brick with color *any*, and *Left in order* of orange bricks is zero. This was
   not sufficient, therefore 4 more bricks of different color (trans-black) were added.
   
   ![](img/htmllog.jpg)

1. Repeat applying order for as many wanted lists as required.

# How to get Wanted list from Bricklink as .xml

1. Open wanted list in Bricklink.

1. Select button *Download* and save .xml file in some directory.

   ![](img/wantedlistdownload.jpg)

# How to get Orders from Bricklink as .xml
1. Open order in Bricklink

1. Almost on the bottom is button *Add order items To My Wanted List*.

1. Select and create new wanted list.

1. Download the order-as-wanted-list into your directory in the same way as a regular wanted list.

   ![](img/ordertowantedlist.jpg)

# How to upload Updated wanted lists back to Bricklink
1. Upload wanted lists back to Bricklink.

   ![](img/uploadwantedlist2.jpg)
