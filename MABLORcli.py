#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# script settings:
# ------- version number:
vernum = '0.1'
# ------- testing - dry run:
dry = False
# ------- use unique filenames:
# set to False only for testing of small number of files!
uniquef = True
# -------

# to set return value to system:
from sys import exit as sysexit
# to get input arguments:
from sys import argv
# to get file info:
import os
# to get unique file names:
import uuid
import ABLORcli


def print_usage(errormsg=''):
    if len(errormsg) > 0:
        print(errormsg)
        print('')
    print('Usage:')
    print('')
    print('python3 MABLORcli.py ' +
            '-o O1.xml O2.xml ... -w W1.xml W2.xml ...')
    print('')
    print('O1.xml - ' +
            'filename of first order (saved as wanted list) that will be ' +
            'applied to wanted list.')
    print('W1.xml - filename of first wanted list, to this the orders ' +
            'will be applied.')
    print('')
    print("The order of Orders and Wanted lists is important.")
    print("First all orders will be applied to first wanted list, next")
    print("all left bricks in all orders will be applied to second wanted")
    print("list etc.")
    print('')
    print("Order and wanted list have to be in bricklink's .xml format.")
    print("Version: " + vernum)
    print('see https://gitlab.com/KaeroDot/applyblorder')

def check_files_exists(wanted, orders):
    # check files (non)existence:
    allfilesok = True
    for w in wanted:
        if not os.path.isfile(w):
            print('Wanted list "' + w + '" was not found!')
            allfilesok = False
    for o in orders:
        if not os.path.isfile(o):
            print('Order "' + w + '" was not found!')
            allfilesok = False
    if not allfilesok:
        print_usage('No wanted lists!')
        sysexit(1)

def check_extract_arguments():
    # show how-to-use message in case of incorrect parameters:
    if len(argv) < 5:
        print_usage()
        sysexit(1)
    # extract input arguments
    # get index of '-o'
    try:
        ostart = argv.index('-o') + 1
    except ValueError:
        print_usage('Missing -o argument!')
        sysexit(1)
    # get index of '-w'
    try:
        wstart = argv.index('-w') + 1
    except ValueError:
        print_usage('Missing -w argument!')
        sysexit(1)
    # get input lists:
    if ostart < wstart:
        oend = wstart - 1
        wend = len(argv)
    else:
        oend = len(argv)
        wend = ostart - 1
    orders = argv[ostart:oend]
    wanted = argv[wstart:wend]
    if len(orders) == 0:
        print_usage('No orders!')
        sysexit(1)
    if len(wanted) == 0:
        print_usage('No wanted lists!')
        sysexit(1)
    return wanted, orders

def multiapply(wanted, orders):
    # returns list of filenames of new wanted lists
    # and list of filenames of new orders

    # list of html logs:
    logs = []
    wanted_trash = []
    orders_trash = []
    logs_trash = []
    # old filenames:
    wanted_old = wanted.copy()
    orders_old = orders.copy()
    # apply orders to all lists:
    for w in range(len(wanted)):
        for o in range(len(orders)):
            # make unique temporary file names
                #  unique_filename = str(uuid.uuid4())

            # apply order to wanted list
            W_in = wanted[w]
            O_in = orders[o]
            if not uniquef:
                W_out, O_out, hlog = ABLORcli.generate_filenames(W_in,
                                                                     O_in)
            else:
                W_out = uuid.uuid4().hex
                O_out = uuid.uuid4().hex
                hlog = uuid.uuid4().hex
            # print action:
            print("applying: " + W_in + "; " + O_in + " -> "
                  + W_out + "; " + O_out + "; " + hlog)
            # make the action:
            if not dry:
                msg, logout = ABLORcli.apply_O_to_W(W_in,
                                                   O_in,
                                                   W_out,
                                                   O_out,
                                                   hlog)
            # add w and o into trash if not source files:
            if o > 0:
                wanted_trash.append(wanted[w])  # same as W_in
            if o < len(orders) - 1:
                logs_trash.append(hlog)  # same as case for wanted
            else:
                logs.append(hlog)  # only final logs here
            if w > 0:
                orders_trash.append(orders[o])  # same as O_in
            # set output files as inputs for next loop iteration:
            wanted[w] = W_out
            orders[o] = O_out
    # clean up - delete all in trash but first ones
    for w in wanted_trash:
        print('removing: ' + w)
        os.remove(w) if not dry else False
    for l in logs_trash:
        print('removing: ' + l)
        os.remove(l) if not dry else False
    for o in orders_trash:
        print('removing: ' + o)
        os.remove(o) if not dry else False
    print(wanted)
    print(orders)
    print(logs)
    # rename final files to something more simple
    wantednew = []
    for i, w in enumerate(wanted):
        D = os.path.dirname(wanted_old[i])
        F, E = os.path.splitext(os.path.basename(wanted_old[i]))
        newfn = os.path.join(D, F + '_new' + E)
        newfnl = os.path.join(D, F + '_new_last_log.html')
        print('renaming: ' + w + ' -> ' + newfn)
        os.rename(w, newfn) if not dry else False
        print('renaming: ' + logs[i] + ' -> ' + newfnl)
        os.rename(logs[i], newfnl) if not dry else False
        wantednew.append(newfn)
    ordersnew = []
    for i, o in enumerate(orders):
        D = os.path.dirname(orders_old[i])
        F, E = os.path.splitext(os.path.basename(orders_old[i]))
        newfn = os.path.join(D, F + '_new' + E)
        print('renaming: ' + o + ' -> ' + newfn)
        os.rename(o, newfn) if not dry else False
        ordersnew.append(newfn)
    return(wantednew, ordersnew)

if __name__ == "__main__":
    wanted, orders = check_extract_arguments()
    check_files_exists(wanted, orders)
    multiapply(wanted, orders)
    # successful end:
    sysexit(0)
