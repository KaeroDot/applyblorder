#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# script assumptions:
# - every brick in specific color is only once in every wanted list
# - there could be multiple same type bricks of NO/ANY color in wanted list
# - there could be only one brick of specific color in order list
# - there could not be bricks of NO/ANY color
# - bricks condition does not matter (because when order is changed to wanted
# list in bricklink, condition is set to "any" - that is X in xml file)
# - this format of Bricklink wanted list (with possible multiple ITEMs):
#   <INVENTORY>
#   <ITEM>
#   <ITEMTYPE>P</ITEMTYPE>
#   <ITEMID>2</ITEMID>
#   <COLOR>1</COLOR>
#   <MAXPRICE>-1.0000</MAXPRICE>
#   <MINQTY>9</MINQTY>
#   <CONDITION>X</CONDITION>
#   <NOTIFY>N</NOTIFY>
#   </ITEM>
#   </INVENTORY>

# ------- version number:
vernum = '0.5'
# -------

# to set return value to system:
from sys import exit as sysexit
# to get input arguments:
from sys import argv
# to get file info:
import os
# xml processing:
import xml.etree.ElementTree as ET
# static parts of html log
import htmllog


def generate_command(W_in, O_in, W_out, O_out, hlog):
    # geneerate console command that can be used to repeat action
    cmd = ('ABLORcli.py' +
           ' ' + O_in +
           ' ' + W_in +
           ' ' + O_out +
           ' ' + W_out +
           ' ' + hlog)
    return cmd


def get_id(root):
    # returns brick type from XML root
    iid = root.find('ITEMID').text
    return iid


def get_color(item):
    # returns brick color from XML root
    color = item.find('COLOR')
    if color is not None:
        return color.text
    else:
        return None


def get_minqty(item):
    # returns minimal qty from XML root
    # for wanted list, it is how much is required
    # for order, it is how much was ordered
    # XXX FIX Not always available, sometimes bricklink generates xml without minqty
    # field.
    minqty = int(item.find('MINQTY').text)
    return minqty


def get_qtyfilled(item):
    # returns filled qty from XML root
    # for wanted list, it is how much already have
    # order probably does not have this field
    qtyf = item.find('QTYFILLED')
    if qtyf is None:
        # field not set, no brick yet buyed and added to wl:
        return 0
    elif qtyf.text is None:
        # field not set, but .text still exist because the subelement
        # was just created and value was not yet assigned
        # return 0:
        return 0
    else:
        return int(qtyf.text)


def make_log_msg(res):
    # convert list res into text message
    msg = ''
    for row in res:
        msg += ('ID ' + row[0] +
                ', color ' + c2t(row[1]) +
                ': want: ' + str(row[2]) +
                ', have: ' + str(row[3]) +
                ', in order: ' + str(row[4]) +
                ', ' + row[5])
        if not not row[6]:
            msg += (': added to WL: ' + str(row[6]) +
                    ', left in order: ' + str(row[7]) +
                    ', new have: ' + str(row[8])
                    )
        msg += '\n'
    msg = msg.replace('<br>', ',')
    return msg


def compareitems(Oitem, Witem):
    # checks if items contain bricks with same id and color
    # ITEMID can be part number, part string (like 3068b)
    # COLOR is probably only number...?
    # so it is better to compare as text
    # ! if wanted item has undefined color, it is considered as match
    # ! if order item has undefined color, it is NOT considered as match
    Oitemid = Oitem.find('ITEMID').text.lower().strip()
    Witemid = Witem.find('ITEMID').text.lower().strip()
    Ocolor = Oitem.find('COLOR')
    if Ocolor is None:
        # order color is not defined, cannot get match:
        return False
    else:
        Ocolor = Ocolor.text.lower().strip()
    Wcolor = Witem.find('COLOR')
    if Wcolor is None:
        # wanted  color is any, therefore colors
        # aromatically should match
        Wcolor = 1  # just any number is ok if same as lower
        Ocolor = 1  # just any number is ok if same as higher
    else:
        # wanted item color is defined
        Wcolor = Wcolor.text.lower().strip()
    # compare ids and colors:
    if Oitemid == Witemid and Ocolor == Wcolor:
        return True
    else:
        return False


def finditem(order, Witem):
    # finds brick Witem from wanted list
    # in order list and return as item
    for Oitem in order.iter('ITEM'):
        if compareitems(Oitem, Witem):
            return Oitem


def findALLitems(order, Witem):
    # finds item Witem (brick from wanted list) in order list for any available
    # color and return as list of items (bricks from order list)
    items = []
    for Oitem in order.iter('ITEM'):
        if compareitems(Oitem, Witem):
            items += [Oitem]
    return items


def processWItemC(Witem, rOi):
    # process items one by one, processing of WL items with color any
    # are moved to the end of processing.
    # returns reslog - array with results
    # variables:
    # lists are needed for multiple found items in order
    # Wrn - Wants
    # Wn - Have
    # On - order number (In order)
    # Ons - order numbers (In order)
    # Ans - Added to WL
    # Wnns - wanted list new number (New have)
    # Onns - order new number, list (Left in order)

    Wcolor = get_color(Witem)
    # get number of bricks required in wanted list:
    Wrn = get_minqty(Witem)
    # get number of bricks already in wanted list:
    Wn = get_qtyfilled(Witem)
    # first part of resrow:
    resrow = [get_id(Witem), Wcolor, str(Wrn), str(Wn)]
    # check number of missing items in WL:
    if Wrn - Wn <= 0:
        # ACTION: wanted list full
        resrow += ['Wanted list full']
        resrow += ['-',
                   '-',
                   '-',
                   '-',
                   '0']
        return resrow
    # find all such bricks in order:
    Oitems = findALLitems(rOi, Witem)
    if len(Oitems) == 0:
        # ACTION: WL brick not in order
        resrow += ['Not found in order']
        #  resrow += ['', '', '', '', '']
        resrow += ['-',
                   '-',
                   '-',
                   '-',
                   str(Wrn - Wn)]
        return resrow
    # brick from WL was found in order
    Ans, Wnns, Ons, Onns, OIds, Ocolors = [], [], [], [], [], []
    # if color was specified in Wl, only one item in Oitems
    # if not, more items are expected
    # (Oitems is not empty because this was managed already)
    for Oitem in Oitems:
        OIds += get_id(Oitem)
        Ocolors += [get_color(Oitem)]
        # get number of bricks in order:
        On = get_minqty(Oitem)
        Ons += [On]
        if On <= 0:
            # ACTION: this item in order is empty
            Ans += [0]
            Wnns += [get_qtyfilled(Witem)]
            Onns += [0]
        else:
            # ACTION: this item - transfer bricks from O to WL
            # number of bricks that will be applied:
            Ans += [min([On, Wrn - Wn])]
            # apply new number to the wanted list:
            transferBricks(Ans[-1], Witem, Oitem)
            # actual number of have bricks in wanted list changed,
            # update it (needed for next iteration evaluation of Ans)
            Wn = get_qtyfilled(Witem)
            # new have in WL:
            Wnns += [Wn]
            # get new number of bricks in order list:
            Onns += [get_minqty(Oitem)]
    # make line for log
    # get last value of number of bricks already in wanted list:
    Wn = get_qtyfilled(Witem)
    if sum(Ans) == 0:
        # all found items in order were empty
        resrow += ['No bricks left in order']
        resrow += ['-',
                   '-',
                   '-',
                   '-',
                   str(Wrn-Wn)]
    elif len(Ans) == 1:
        # single brick transfer
        if Wcolor is None:
            # transfer to WL item with any color
            resrow += ['Bricks of color ' + str(Ocolors[0]) + ' transferred.']
        else:
            # transfer to WL item with specific color
            resrow += ['Bricks transferred']
        resrow += [str(Ons[0]),
                   str(Ans[0]),
                   str(Onns[0]),
                   str(Wnns[0]),
                   str(Wrn-Wn)]
    else:
        # multiple brick transfer
        tmp = 'Bricks transferred,'
        for i in range(len(Ans)):
            tmp += '<br> %d pcs of color %s:' % (Ans[i], Ocolors[i])
        Ons = '<br>' + '<br>'.join([str(i) for i in Ons])
        Ans = '<br>' + '<br>'.join([str(i) for i in Ans])
        Onns = '<br>' + '<br>'.join([str(i) for i in Onns])
        resrow += [tmp,
                   Ons,
                   Ans,
                   Onns,
                   str(Wnns[-1]),
                   str(Wrn-Wn)]
    return resrow


def transferBricks(An, Witem, Oitem):
    # transfer bricks from order item Oitem to wanted
    # list item Witem by value An
    # wanted list value:
    # ensure element QTYfILLED exist:
    Qt = Witem.find('QTYFILLED')
    if Qt is None:
        Qt = ET.SubElement(Witem, 'QTYFILLED')
    # set wanted list value:
    Qt.text = str(get_qtyfilled(Witem) + An)
    # order value:
    Oitem.find('MINQTY').text = str(get_minqty(Oitem) - An)


def c2t(color):
    # converts color to text
    # accounts for possible None value
    if color is None:
        return 'any'
    else:
        return color


def apply_O_to_W(W_in, O_in, W_out, O_out, hlog):
    # iterate through all wanted list items
    # find if item is in order
    # add maximum possible quantity to wanted list and remove from order

    # array with results for log and summary message:
    # collumn order:
    # id, color, WL wants, WL have, O have, action, WL added,
    # WL new have, O new have, missing
    # XXX should be changed into list, will be better
    res = []

    # load wanted list and order:
    # r as root, W as Wanted list, O as order, i as input, o as output
    rWi = ET.parse(W_in)
    rOi = ET.parse(O_in)

    makelast = []
    # iterate:
    for Witem in rWi.iter('ITEM'):
        color = get_color(Witem)
        # check if this item has color any. if so, this item will be solved as
        # last one. because such item in order can be aimed for specific item
        # in wanted list later.
        if color is None:
            # do this later
            makelast += [Witem]
            print('Item ' + get_id(Witem) + '/' + c2t(color) +
                  ' will be evaluated at the end.')
        else:
            resrow = processWItemC(Witem, rOi)
            # add last table row to table:
            res.append(resrow)
    # now do the bricks in WL with any color
    for Witem in makelast:
        resrow = processWItemC(Witem, rOi)
        # add last table row to table:
        res.append(resrow)
    # write out outputs
    rWi.write(W_out)
    rOi.write(O_out)
    # make summary message:
    msg = make_log_msg(res)
    # show summary message:
    cmd = generate_command(W_in, O_in, W_out, O_out, hlog)
    print(cmd)
    print('Result:')
    print(msg)
    # make html log:
    res2 = list(res)
    # get rid of None values for color:
    for i in range(len(res2)):
        res2[i][1] = c2t(res2[i][1])
    w = htmllog.make_log_html(res, cmd, hlog)
    # write html log
    f = open(hlog, 'w', encoding='utf8')
    f.write(w)
    f.close()
    # return summary message and html log:
    return msg, w


def generate_filenames(W_in, O_in):
    # generate W_out, O_out and hltmlog filenames from W_in and O_in
    Wd = os.path.dirname(W_in)
    Wf, We = os.path.splitext(os.path.basename(W_in))
    Od = os.path.dirname(O_in)
    Of, Oe = os.path.splitext(os.path.basename(O_in))
    W_out = os.path.join(Wd, Wf + '+' + Of + We)
    O_out = os.path.join(Od, Of + '-' + Wf + Oe)
    hlog = generate_logfilename(W_out)
    return W_out, O_out, hlog


def generate_logfilename(W_out):
    # generate log filename for the case W_out and O_out are set by user
    Wd = os.path.dirname(W_out)
    Wf, We = os.path.splitext(os.path.basename(W_out))
    hlog = os.path.join(Wd, Wf + '=LOG.html')
    return hlog


def check_files(W_in, O_in, W_out, O_out):
    msg = ''
    exitv = 0
    if not os.path.isfile(W_in):
        msg += ('Wanted list\n"' + W_in + '"\nwas not found!\n')
        exitv = 1
    if not os.path.isfile(O_in):
        msg += ('Order\n"' + O_in + '"\nwas not found!\n')
        exitv = 1
    if os.path.isfile(W_out):
        msg += ('Wanted list\n"' + W_out + '"\nalready exist!\n')
        exitv = 2
    if os.path.isfile(O_out):
        msg += ('Order\n"' + O_out + '"\nalready exist!\n')
        exitv = 2
    return exitv, msg

if __name__ == "__main__":
    # show how-to-use message in case of incorrect parameters:
    if len(argv) < 3 or len(argv) > 6:
        print('Usage:')
        print('')
        print('python3 apply_order.py ' +
              'O_in.xml W_in.xml [LOG.html]')
        print('python3 apply_order.py ' +
              'O_in.xml W_in.xml O_out.xml W_out.xml [LOG.html]')
        print('')
        print('O_in.xml - ' +
              'filename of order (saved as wanted list) that will be ' +
              'applied to wanted list.')
        print('W_in.xml - filename of wanted list, to this the order ' +
              'will be applied.')
        print('O_out.xml - ' +
              'filename for new order that will be generated, ' +
              'without bricks applied to the wanted list')
        print('W_out.xml - ' +
              'filename for new wanted list that will be generated. ' +
              'with applied order')
        print('LOG.html - ' +
              'output log as html')
        print('')
        print("Order and wanted list have to be in bricklink's .xml format.")
        print("Version: " + vernum)
        print('see https://gitlab.com/KaeroDot/applyblorder')
        sysexit(1)
    else:
        # extract input arguments
        O_in = argv[1]
        W_in = argv[2]
        if len(argv) == 3:
            W_out, O_out, hlog = generate_filenames(W_in, O_in)
        elif len(argv) == 4:
            hlog = argv[3]
            W_out, O_out, hlog = generate_filenames(W_in, O_in)
        elif len(argv) == 5:
            # override O_out and W_out:
            O_out = argv[3]
            W_out = argv[4]
            # get htmllog:
            hlog = generate_logfilename(W_out)
        elif len(argv) == 6:
            O_out = argv[3]
            W_out = argv[4]
            hlog = argv[5]
        # check files (non)existence:
        exitv, msg = check_files(W_in, O_in, W_out, O_out)
        # htmllog will be always overwritten
        if exitv:
            print(msg)
            exit(exitv)
        else:
            # apply order to wanted list:
            msg, web = apply_O_to_W(W_in, O_in, W_out, O_out, hlog)
        # successful end
