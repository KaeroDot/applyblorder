# how to make 'ablor.png'
1. In svg:
        1. make changes
        2. set document size acording rectangle in layer 'ablor page size'
        3. set layer 'ablor page size' as visible
        4. set layer 'ablor' as visible
        5. export png image, area page, filename 'results/ablor.png'
2. result is in 'results/ablor.gif'

# how to make 'mablor.gif'
1. In svg:
        1. make changes
        2. set document size acording rectangle in layer 'mablor page size'
        3. set layer 'mablor page size' as visible
        4. set layer 'mablor ani 1' as visible
        5. export png image, area page, filename './mablor01.png'
        6. set leayer 'mablor ani 2' as visible
        7. export png image, area page, filename './mablor02.png'
        8. set leayer 'mablor ani 2' as invisible
        9. set leayer 'mablor ani 3' as visible
        10. export png image, area page, filename './mablor03.png'
        11. repeat 7-9 for layers 'mablor ani 4' to 'mablor ani 10'
2. run 'make_gif.sh'
3. result is in 'results/mablor.gif'
