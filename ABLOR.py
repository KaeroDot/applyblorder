#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
from webbrowser import open as webopen
import ABLORcli
import wx
from GUI.ABLOR_GUI import mainframeclass, resultframeclass
# to set return value to system:
from sys import exit as sysexit


class clsFileDropTarget(wx.FileDropTarget):
    # class to enable (drag&)drop of files into the control
    def __init__(self, window):
        wx.FileDropTarget.__init__(self)
        self.window = window

    def OnDropFiles(self, x, y, filenames):
        f = os.path.splitext(os.path.relpath(filenames[0], os.curdir))
        self.window.SetValue(f[0])
        return True


class mainframe(mainframeclass):
    def __init__(self, *args, **kwds):
        mainframeclass.__init__(self, *args, **kwds)
        # insert more initialization code here
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.Bind(wx.EVT_TEXT,  self.txtW_in_textchange, self.txtW_in)
        self.Bind(wx.EVT_TEXT,  self.txtO_in_textchange, self.txtO_in)
        self.Bind(wx.EVT_TEXT,  self.txtW_out_textchange, self.txtW_out)
        self.Bind(wx.EVT_TEXT,  self.txtO_out_textchange, self.txtO_out)
        # initialize values of W_out and O_out by using already event handlers:
        self.txtO_in.SetValue(self.txtO_in.GetValue())
        # initialize drop targets:
        self.txtW_in.SetDropTarget(clsFileDropTarget(self.txtW_in))
        self.txtO_in.SetDropTarget(clsFileDropTarget(self.txtO_in))
        self.txtW_out.SetDropTarget(clsFileDropTarget(self.txtW_out))
        self.txtO_out.SetDropTarget(clsFileDropTarget(self.txtO_out))

    def OnClose(self, event):
        ABLOR.resultframe.Destroy()
        self.Destroy()

    def txtW_in_textchange(self, event):
        global Woutchanged
        global Ooutchanged
        if not(Woutchanged or Ooutchanged):
            W_out, O_out, htmllog = ABLORcli.generate_filenames(
                self.txtW_in.GetValue(), self.txtO_in.GetValue())
            self.txtW_out.ChangeValue(W_out)
            self.txtO_out.ChangeValue(O_out)

    def txtO_in_textchange(self, event):
        global Woutchanged
        global Ooutchanged
        if not(Woutchanged or Ooutchanged):
            W_out, O_out, htmllog = ABLORcli.generate_filenames(
                self.txtW_in.GetValue(), self.txtO_in.GetValue())
            self.txtW_out.ChangeValue(W_out)
            self.txtO_out.ChangeValue(O_out)

    def txtW_out_textchange(self, event):
        global Woutchanged
        Woutchanged = True

    def txtO_out_textchange(self, event):
        global Ooutchanged
        Ooutchanged = True

    def btnW_in_clicked(self, event):
        global Woutchanged
        global Ooutchanged
        fd = wx.FileDialog(self, "Open wanted list file",
                           wildcard="XML files (*.xml)|*.xml",
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if fd.ShowModal() != wx.ID_CANCEL:
            f = os.path.splitext(os.path.relpath(fd.GetPath(), os.curdir))
            self.txtW_in.SetValue(f[0])

    def btnO_in_clicked(self, event):
        global Woutchanged
        global Ooutchanged
        fd = wx.FileDialog(self, "Open order list file",
                           wildcard="XML files (*.xml)|*.xml",
                           style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if fd.ShowModal() != wx.ID_CANCEL:
            f = os.path.splitext(os.path.relpath(fd.GetPath(), os.curdir))
            self.txtO_in.SetValue(f[0])

    def btnW_out_clicked(self, event):
        global Woutchanged
        fs = wx.SaveFileSelector("Select filename for modified wanted list",
                                 extension=".xml",
                                 default_name="O.xml")
        if fs:
            f = os.path.splitext(os.path.relpath(fs, os.curdir))
            self.txtW_out.SetValue(f[0])
            Woutchanged = True

    def btnO_out_clicked(self, event):
        global Ooutchanged
        fs = wx.SaveFileSelector("Select filename for modified order list",
                                 extension=".xml",
                                 default_name="O.xml")
        if fs:
            f = os.path.splitext(os.path.relpath(fs, os.curdir))
            self.txtO_out.SetValue(f[0])
            Ooutchanged = True

    def btnQuit_clicked(self, event):
        ABLOR.resultframe.Destroy()
        self.Destroy()

    def btnAbout_clicked(self, event):
        webopen('https://gitlab.com/KaeroDot/applyblorder')

    def btnStart_clicked(self, event):
        # button start pressed
        # add extension to file names:
        W_in = str(self.txtW_in.GetValue()) + '.xml'
        O_in = str(self.txtO_in.GetValue()) + '.xml'
        W_out = str(self.txtW_out.GetValue()) + '.xml'
        O_out = str(self.txtO_out.GetValue()) + '.xml'
        # show debug messsage:
        print('Applying:\nABLORcli.py' +
              ' ' + O_in + ' ' + W_in +
              ' ' + O_out + ' ' + W_out +
              '\nResult:')
        # check files (non)existence:
        exitv, msg = ABLORcli.check_files(W_in, O_in, W_out, O_out)
        if exitv == 1:
            # input file not found
            dialog = wx.MessageDialog(self,
                                      'Input file not found.',
                                      'File error!',
                                      wx.OK | wx.ICON_EXCLAMATION)
            reply = dialog.ShowModal()
            return
        elif exitv == 2:
            # output files exist, show message dialog
            print(msg)
            dialog = wx.MessageDialog(self,
                                      msg + '\n Delete file(s)?',
                                      'File error!',
                                      wx.OK | wx.CANCEL | wx.CANCEL_DEFAULT | wx.ICON_EXCLAMATION)
            reply = dialog.ShowModal()
            if reply == wx.ID_CANCEL:
                return
            else:
                # delete files and continue
                try:
                    os.remove(W_out)
                    os.remove(O_out)
                except FileNotFoundError:
                    # ignore file not found error:
                    pass
        try:
            # apply order to wanted list
            # get htmllog:
            htmllog = ABLORcli.generate_logfilename(W_out)
            # and make the magic:
            msg, w = ABLORcli.apply_O_to_W(W_in,
                                           O_in,
                                           W_out,
                                           O_out, htmllog)
        except Exception as e:
            errmsg = ('Following error occured: \n' +
                      str(e) +
                      '\nFix it yourself or ask author.')
            print(errmsg)
            # dialog showing error to user:
            dialog = wx.MessageDialog(self,
                                      errmsg,
                                      'Software error!',
                                      wx.OK | wx.ICON_ERROR)
            dialog.ShowModal()
            # quit software after and error:
            sysexit(1)
        # prepare result frame and switch to it:
        self.Hide()
        ABLOR.resultframe.txtResult.SetValue(msg)
        ABLOR.resultframe.htmllog = htmllog
        ABLOR.resultframe.Show()


class resultframe(resultframeclass):
    def __init__(self, *args, **kwds):
        resultframeclass.__init__(self, *args, **kwds)
        # insert more initialization code here
        self.Bind(wx.EVT_CLOSE, self.OnClose)
        self.btnQuit.Bind(wx.EVT_CLOSE, self.OnClose)
        self.htmllog = ''

    def OnClose(self, event):
        ABLOR.mainframe.Destroy()
        self.Destroy()

    def btnContinue_clicked(self, event):
        global Woutchanged
        global Ooutchanged
        # reset status of changed:
        Woutchanged = False
        Ooutchanged = False
        # apply new names to the texts
        ABLOR.mainframe.txtO_in.SetValue(
            ABLOR.mainframe.txtO_out.GetValue())
        ABLOR.mainframe.txtW_in.SetValue('')
        ABLOR.mainframe.txtO_out.SetValue('')
        ABLOR.mainframe.txtW_out.SetValue('')
        self.Hide()
        ABLOR.mainframe.Show()

    def btnCopyclip_clicked(self, event):
        if wx.TheClipboard.Open():
            wx.TheClipboard.SetData(
                wx.TextDataObject(self.txtResult.GetValue()))
            wx.TheClipboard.Close()

    def btnOpenLOG_clicked(self, event):
        webopen(self.htmllog)

    def btnCopyW_clicked(self, event):
        if wx.TheClipboard.Open():
            f = open(ABLOR.mainframe.txtO_out.GetValue() + '.xml', 'r')
            wx.TheClipboard.SetData(wx.TextDataObject(f.read()))
            wx.TheClipboard.Close()

    def btnQuit_clicked(self, event):
        ABLOR.mainframe.Destroy()
        self.Destroy()


class ABLOR_GUIclass(wx.App):
    def OnInit(self):
        self.mainframe = mainframe(None, wx.ID_ANY, "")
        self.resultframe = resultframe(None, wx.ID_ANY, "")
        self.SetTopWindow(self.mainframe)
        self.mainframe.Show()
        return True


if __name__ == "__main__":
    Ooutchanged = False
    Woutchanged = False
    ABLOR = ABLOR_GUIclass(0)
    ABLOR.MainLoop()
