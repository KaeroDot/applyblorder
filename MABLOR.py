#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import wx
import pickle
from GUI.MABLOR_GUI import mainframeclass
from MABLORcli import multiapply


class clsFileDropTarget(wx.FileDropTarget):
    # class to enable (drag&)drop of files into the grid control
    def __init__(self, window, name):
        wx.FileDropTarget.__init__(self)
        self.window = window
        self.name = name

    def OnDropFiles(self, x, y, filenames):
        # process dropping action:
        for f in filenames:
            # only drop if xml file:
            tmp, extension = os.path.splitext(f)
            if extension == '.xml':
                if self.name == 'grid_W':
                    # dropping into wanted list
                    MABLOR.mainframe.add_W_files([f])
                elif self.name == 'grid_O':
                    # dropping into orders list
                    MABLOR.mainframe.add_O_files([f])
        return True


class mainframe(mainframeclass):
    def __init__(self, *args, **kwds):
        mainframeclass.__init__(self, *args, **kwds)
        self.load_config()

        # initialize drop targets:
        self.grid_W.SetDropTarget(clsFileDropTarget(self.grid_W, 'grid_W'))
        self.grid_O.SetDropTarget(clsFileDropTarget(self.grid_O, 'grid_O'))

        # bind own size event:
        self.Bind(wx.EVT_SIZE, self.onResize, self)
        self.Bind(wx.EVT_MAXIMIZE, self.onResize, self)
        self.Bind(wx.EVT_CLOSE, self.onClose, self)
        self.Update

    def get_config_filepath(self):
        # make configuration file path:
        return os.path.join(os.path.dirname(os.path.abspath(__file__)),
                            'MABLOR.cfg')

    def load_config(self):
        # configuration file path:
        f = self.get_config_filepath()
        # check if settings file exists:
        if os.path.exists(f):
            try:
                config = pickle.load(open(f, "rb"))
                # update lists:
                self.Wlist = config[0]
                self.Olist = config[1]
                # update grids:
                for f in self.Wlist:
                    self.set_grid_W_line(f)
                for f in self.Olist:
                    self.set_grid_O_line(f)
                self.chck_rem_fullWitems.SetValue(config[2])
                self.chck_rem_emptyOitems.SetValue(config[3])
            except:
                self.Wlist = []
                self.Olist = []
                self.chck_rem_fullWitems.SetValue(False)
                self.chck_rem_emptyOitems.SetValue(False)
        else:
            # if no settings:
            self.Wlist = []
            self.Olist = []

    def save_config(self):
        # configuration file path:
        f = self.get_config_filepath()
        # write configuration:
        pickle.dump([self.Wlist,
                     self.Olist,
                     self.chck_rem_fullWitems.GetValue(),
                     self.chck_rem_emptyOitems.GetValue()
                     ], open(f, "wb"))

    def onResize(self, event):
        # overloaded because of resizing columns in grid
        # layout must be called now because we need new grid sizes:
        self.Layout()
        # set width of collumns with filenames in grid_W
        sz = self.grid_W.GetSize()[0] \
            - self.grid_W.GetRowLabelSize() \
            - self.grid_W.GetColSize(0) \
            - self.grid_W.GetColSize(1) \
            - self.grid_W.GetColSize(3)
        if sz < 1:
            sz = 1
        self.grid_W.SetColSize(2, sz)
        # set width of collumns with filenames in grid_O
        sz = self.grid_O.GetSize()[0] \
            - self.grid_O.GetRowLabelSize() \
            - self.grid_O.GetColSize(0) \
            - self.grid_O.GetColSize(1) \
            - self.grid_O.GetColSize(3)
        if sz < 1:
            sz = 1
        self.grid_O.SetColSize(2, sz)
        # set width of collumns with filenames in grid_Wnew
        sz = self.grid_Wnew.GetSize()[0] - self.grid_Wnew.GetRowLabelSize()
        if sz < 1:
            sz = 1
        self.grid_Wnew.SetColSize(0, sz)
        # set width of collumns with filenames in grid_Onew
        sz = self.grid_Onew.GetSize()[0] - self.grid_Onew.GetRowLabelSize()
        if sz < 1:
            sz = 1
        self.grid_Onew.SetColSize(0, sz)
        event.Skip()

    def onClose(self, event):
        self.save_config()
        event.Skip()

    def add_W_files(self, filenames):
        # check wlist if files already exit
        # if not, add line to grid:
        for f in filenames:
            if not self.Wlist.count(f):
                # filename not in list, add it
                # as relative path:
                # into Wlist add full path,
                # into grid only relative path or name?
                self.Wlist.append(f)
                self.set_grid_W_line(os.path.basename(f))

    def set_grid_W_line(self, filename, row=-1):
        # set value of one grid_W row
        # makes new row if row is negative
        # otherwise replace row with new content
        if row < 0:
            self.grid_W.AppendRows(1)
            row = self.grid_W.GetNumberRows() - 1
        self.grid_W.SetCellValue(row, 0, '↑')
        self.grid_W.SetCellValue(row, 1, '↓')
        self.grid_W.SetCellValue(row, 2, os.path.basename(filename))
        self.grid_W.SetCellValue(row, 3, '×')

    def rem_grid_W_line(self, row=-1):
        # remove one grid_W row
        self.grid_W.DeleteRows(row, 1)

    def add_O_files(self, filenames):
        # check wlist if files already exit
        # if not, add line to grid:
        for f in filenames:
            if not self.Olist.count(f):
                # filename not in list, add it
                # as relative path:
                # into Olist add full path,
                # into grid only relative path or name?
                self.Olist.append(f)
                self.set_grid_O_line(os.path.basename(f))

    def set_grid_O_line(self, filename, row=-1):
        # set value of one grid_O row
        # makes new row if row is negative
        # otherwise replace row with new content
        if row < 0:
            self.grid_O.AppendRows(1)
            row = self.grid_O.GetNumberRows() - 1
        self.grid_O.SetCellValue(row, 0, '↑')
        self.grid_O.SetCellValue(row, 1, '↓')
        self.grid_O.SetCellValue(row, 2, os.path.basename(filename))
        self.grid_O.SetCellValue(row, 3, '×')

    def rem_grid_O_line(self, row=-1):
        # remove one grid_O row
        self.grid_O.DeleteRows(row, 1)

    def btn_loadw_clicked(self, event):
        # ask user for files to load and add them into grid_W
        with wx.FileDialog(self,
                           "Open file(s) with wanted list(s)",
                           wildcard="Bricklink xml file (*.xml)|*.xml",
                           style=wx.FD_OPEN |
                           wx.FD_FILE_MUST_EXIST |
                           wx.FD_MULTIPLE) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind
            # Get list of files selected by user:
            filenames = fileDialog.GetPaths()
            self.add_W_files(filenames)

    def btn_loado_clicked(self, event):
        # ask user for files to load and add them into grid_O
        with wx.FileDialog(self,
                           "Open file(s) with order(s)",
                           wildcard="Bricklink xml file (*.xml)|*.xml",
                           style=wx.FD_OPEN |
                           wx.FD_FILE_MUST_EXIST |
                           wx.FD_MULTIPLE) as fileDialog:
            if fileDialog.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind
            # Get list of files selected by user:
            filenames = fileDialog.GetPaths()
            self.add_O_files(filenames)

    def grid_W_cell_clicked(self, event):
        if event.GetCol() == 0:
            # promote row only if clicked on proper row
            if self.grid_W.GetNumberRows() > 1 and event.GetRow() > 0:
                p1 = event.GetRow()
                p2 = event.GetRow() - 1
                # switch elements:
                self.Wlist[p1], self.Wlist[p2] = self.Wlist[p2], self.Wlist[p1]
                self.set_grid_W_line(self.Wlist[p1], p1)
                self.set_grid_W_line(self.Wlist[p2], p2)
        elif event.GetCol() == 1:
            # demote row only if clicked on proper row
            if self.grid_W.GetNumberRows() > 1 \
                    and event.GetRow() < self.grid_W.GetNumberRows() - 1:
                p1 = event.GetRow()
                p2 = event.GetRow() + 1
                # switch elements:
                self.Wlist[p1], self.Wlist[p2] = self.Wlist[p2], self.Wlist[p1]
                self.set_grid_W_line(self.Wlist[p1], p1)
                self.set_grid_W_line(self.Wlist[p2], p2)
        elif event.GetCol() == 2:
            # What to do? nothing?
            # XXX maybe show html list
            pass
        elif event.GetCol() == 3:
            # remove row
            self.Wlist.pop(event.GetRow())
            self.rem_grid_W_line(event.GetRow())

    def grid_O_cell_clicked(self, event):
        if event.GetCol() == 0:
            # promote row only if clicked on proper row
            if self.grid_O.GetNumberRows() > 1 and event.GetRow() > 0:
                p1 = event.GetRow()
                p2 = event.GetRow() - 1
                # switch elements:
                self.Olist[p1], self.Olist[p2] = self.Olist[p2], self.Olist[p1]
                self.set_grid_O_line(self.Olist[p1], p1)
                self.set_grid_O_line(self.Olist[p2], p2)
        elif event.GetCol() == 1:
            # demote row only if clicked on proper row
            if self.grid_O.GetNumberRows() > 1 \
                    and event.GetRow() < self.grid_O.GetNumberRows() - 1:
                p1 = event.GetRow()
                p2 = event.GetRow() + 1
                # switch elements:
                self.Olist[p1], self.Olist[p2] = self.Olist[p2], self.Olist[p1]
                self.set_grid_O_line(self.Olist[p1], p1)
                self.set_grid_O_line(self.Olist[p2], p2)
        elif event.GetCol() == 2:
            # What to do? nothing?
            # XXX maybe show html list
            pass
        elif event.GetCol() == 3:
            # remove row
            self.Olist.pop(event.GetRow())
            self.rem_grid_O_line(event.GetRow())

    def btn_help_clicked(self, event):
        print("2DO")
        print(self.chck_rem_emptyOitems.GetValue())
        print(self.chck_rem_fullWitems.GetValue())
        event.Skip()

    def btn_Wnewdir_clicked(self, event):
        # XXX NOT USED FOR NOW
        dlg = wx.DirDialog(None, "Choose output directory", "",
                           wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST)
        if dlg.ShowModal() == wx.ID_CANCEL:
                return     # the user changed their mind
        self.txt_Wnewdir.SetValue(dlg.GetPath())

    def btn_start_clicked(self, event):
        # check all ok
        if len(self.Wlist) == 0:
            # one of lists empty, show dialog:
            wx.MessageBox("List of Wanted lists is empty.",
                          "Wanted lists missing",
                          wx.OK | wx.ICON_ERROR)
            return
        if len(self.Olist) == 0:
            # one of lists empty, show dialog:
            wx.MessageBox("List of Orders is empty.",
                          "Orders missing",
                          wx.OK | wx.ICON_ERROR)
        # disable buttons:
        for widget in self.GetChildren():
            widget.Disable()
        # set new label for button
        oldlabel = self.btn_start.GetLabel()
        self.btn_start.SetLabel('Working ...')
        wx.Yield()

        # make the magic here:
        wanted = self.Wlist.copy()
        orders = self.Olist.copy()
        newwanted, neworders = multiapply(wanted, orders)

        # load results
        self.grid_Wnew.ClearGrid()
        self.grid_Wnew.AppendRows(len(newwanted))
        for i, fn in enumerate(newwanted):
            print(fn)
            self.grid_Wnew.SetCellValue(i, 0, os.path.basename(fn))
        self.grid_Onew.ClearGrid()
        self.grid_Onew.AppendRows(len(neworders))
        for i, fn in enumerate(neworders):
            print(fn)
            self.grid_Onew.SetCellValue(i, 0, os.path.basename(fn))

        # before enabling buttons, yield to consume all clicks on disabled
        # buttons (otherwise the clicks will wait till end of this function and
        # buttons will be clicked):
        wx.Yield()
        # enable buttons:
        for widget in self.GetChildren():
            widget.Enable()
        # revert label
        self.btn_start.SetLabel(oldlabel)

    def btn_quit_clicked(self, event):
        self.save_config()
        self.Destroy()


class MABLOR_GUIclass(wx.App):
    def OnInit(self):
        # use overloaded class mainframe instead of mainframeclass from
        # wxglade file:
        self.mainframe = mainframe(None, wx.ID_ANY, "")
        self.SetTopWindow(self.mainframe)
        self.mainframe.Show()
        return True

if __name__ == "__main__":
    MABLOR = MABLOR_GUIclass(0)
    MABLOR.MainLoop()
