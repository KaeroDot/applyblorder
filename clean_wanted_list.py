#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# Script removes all full items from wanted list in bricklink format. Full item
# is such that field 'have' is equal or greater than 'want'.

# script assumptions:
# same as applyBLorder.py

# ------- version number:
vernum = '0.1'
# -------

# to set return value to system:
from sys import exit as sysexit
# to get input arguments:
from sys import argv
# to get file info:
import os
# xml processing:
import xml.etree.ElementTree as ET
# read have, want fields:
from applyBLorder import get_qtyfilled
from applyBLorder import get_minqty


def clean_W(W_in, W_out):
    # parse input xml:
    tree = ET.parse(W_in)
    root = tree.getroot()
    # list of removed item ids:
    remitems = []
    for item in tree.findall('ITEM'):
        if get_qtyfilled(item) >= get_minqty(item):
            remitems += [get_itemid(item)]
            # remove item
            root.remove(item)
    msg = 'Removed item ids: '
    for item in remitems:
        msg += ', ' + item
    tree.write(W_out)
    return msg


def get_itemid(item):
    # returns item id from XML
    itemid = item.find('ITEMID').text
    return itemid


def generate_filenames(W_in):
    # generate W_out from W_in
    Wd = os.path.dirname(W_in)
    Wf, We = os.path.splitext(os.path.basename(W_in))
    W_out = os.path.join(Wd, Wf + '_cleaned' + We)
    return W_out


def check_files(W_in, W_out):
    msg = ''
    exitv = 0
    if not os.path.isfile(W_in):
        msg += ('Wanted list\n"' + W_in + '"\nwas not found!\n')
        exitv = 1
    if os.path.isfile(W_out):
        msg += ('Wanted list\n"' + W_out + '"\nalready exist!\n')
        exitv = 2
    return exitv, msg


if __name__ == "__main__":
    # show how-to-use message in case of incorrect parameters:
    if len(argv) < 2 or len(argv) > 3:
        print('Usage:')
        print('')
        print('python3 clean_wanted_list.py ' +
              'W_in.xml')
        print('python3 clean_wanted_list.py ' +
              'W_in.xml W_out.xml')
        print('')
        print('W_in.xml - filename of wanted list that will be cleaned up')
        print('W_out.xml - ' +
              'filename for new wanted list that will be generated.')
        print('')
        print("Script removes all full items from W_in.xml. Full item is" +
              " such that field 'have' is equal or greater than 'want'.")
        print("Version: " + vernum)
        print('see https://gitlab.com/KaeroDot/applyblorder')
        sysexit(1)
    else:
        # extract input arguments
        W_in = argv[1]
        if len(argv) == 2:
            W_out = generate_filenames(W_in)
        elif len(argv) == 3:
            W_out = argv[2]
        # check files (non)existence:
        exitv, msg = check_files(W_in, W_out)
        # htmllog will be always overwritten
        if exitv:
            print(msg)
            exit(exitv)
        else:
            # apply order to wanted list:
            print('Cleaning ' + W_in + ' to ' + W_out)
            msg = clean_W(W_in, W_out)
            print(msg)
        # successful end
