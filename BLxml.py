#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Library for processing Bricklink's xml files containing Wanted lists or
# Orders.
#
# Library assumptions:
# 1, Every brick of specific color is only once in every xml list.

# Bricklink XML description
#
# Documentation from:
#   https://www.bricklink.com/help.asp?helpID=207
#
#  ITEMTYPE: Required: Yes. S for a set, P for a part, M for a minifigure, B
#            for a book, G for a gear item, C for a catalog, I for an
#            instruction manual, O for an original box.
#  ITEMID: Required: Yes. Item number.
#  COLOR: Required: No. Item color ID from the Color Guide.
#  MAXPRICE: Required: No. Maximum price. Number greater than 0.
#  MINQTY: Required: No. Minimum wanted quantity in Wanted list. Ordered amount
#          in Order list.
#          amount.Number greater than 0.
#  QTYFILLED: Required: No. Already have quantity (in Wanted list).
#             (? Number >= 0 ?)
#  CONDITION: Required: No. Specific condition. N for new or U for used.
#  REMARKS: Required: No. Comment visible privately. Any text. Value can not
#           directly contain HTML tags (e.g. <b></b>) or stand-alone ampersands
#           (&). Instead, the following HTML character entities must be used:
#           &lt; in place of a less than sign, &gt; in place of a greater than
#           sign, and &amp; in place of an ampersand.
#  NOTIFY: Required: No. Notification by email is wanted. N for no, Y for yes.
#  WANTEDSHOW: Required: No. Show in Items For Sale Queries. Leave blank or
#              enter Y for this item to be included in item for sale queries or
#              enter N for this item to not be included in item for sale
#              queries.
#  WANTEDLISTID: Required: No. ID of my Additional Wanted List (Field
#                Explanation). Omitting this tag will upload the item to your
#                main wanted list. Enter the ID of your wanted list you wish to
#                upload this item to.
#
#  Sample Wanted List in BrickLink XML format:
#  <INVENTORY>
#    <ITEM>
#      <ITEMTYPE>P</ITEMTYPE>
#      <ITEMID>3622</ITEMID>
#      <COLOR>11</COLOR>
#      <QTYFILLED>4</QTYFILLED>
#    </ITEM>
#    <ITEM>
#      <ITEMTYPE>P</ITEMTYPE>
#      <ITEMID>3039</ITEMID>
#    </ITEM>
#    <ITEM>
#      <ITEMTYPE>P</ITEMTYPE>
#      <ITEMID>3001</ITEMID>
#      <COLOR>5</COLOR>
#      <MAXPRICE>1.00</MAXPRICE>
#      <MINQTY>100</MINQTY>
#      <CONDITION>N</CONDITION>
#      <REMARKS>for MOC AB154A</REMARKS>
#      <NOTIFY>N</NOTIFY>
#    </ITEM>
#  </INVENTORY>
#
# This library use for temporary storage a LIBDATA tag in BLitem. It can
# contain anything as needed. It is usually json serialized data.

# ------- version number:
VERSION = '0.1'
# Filenames of templates for html list. Files must be in resources directory:
LIST_TEMPLATE_FILENAMES = ['list-template-A.html',
                           'list-template-B.html',
                           'list-template-C.html']
# global variable with lego catalogue, so reading of xml files is done only
# once, to save memory and increase speed.
LC = None
# -------

import os
import sys
# xml processing:
import xml.etree.ElementTree as ET
# typing checks:
from typing import List, Optional, Union, Dict
# to create a copy
import copy
# for storing library private data into BLitem xml structure:
import json


class LegoCatalogue():
    """Class for parsing xml data from bricklink. Is used to
    convert color id or part id into color name, hexadecimal representation of
    the color, or part name."""
    # resources were obtained by downloading from bricklink.
    # how to donwload xml data files:
    # go to bricklink, login, catalogue->download

    def __init__(self, resource_dir: str) -> None:
        """Initialize class. Path to xml files with resources has to be
        provided."""
        # input: resourcedir is a directory with files colors.xml and Parts.xml
        self._is_initialized = False
        self._resource_dir = resource_dir
        # only possible values of item type in BL item:
        self._item_type_values = ['S', 'P', 'M', 'B', 'G', 'C', 'I', 'O']

    def _initialize(self) -> None:
        """Initialize instance: parse resource xml files. This is run by class
        itself when needed."""
        self.colors = ET.parse(os.path.join(self._resource_dir, 'colors.xml'))
        self.parts = ET.parse(os.path.join(self._resource_dir, 'Parts.xml'))
        self._is_initialized = True

    def color_name(self, colorid: Optional[int]) -> str:
        """Return color name of color defined by color ID."""
        # input: int with brick color identification number
        # output: string with color name based on colorid value
        if not self._is_initialized:
            self._initialize()
        if colorid is None:
            return ''
        for item in self.colors.iter('ITEM'):
            try:
                c = item.find('COLOR')  # tag with color number
                if c is not None and c.text is not None:
                    if colorid == int(c.text):
                        cn = item.find('COLORNAME')
                        if cn is not None and cn.text is not None:
                            return str(cn.text)
            except ValueError:
                pass
        return 'Unknown color'

    def color_hex(self, colorid: Optional[int]) -> str:
        """Return hexadecimal value of color defined by color ID."""
        # input: str/int with brick color identification number
        # output: string with color hexadecimal value based on colorid value
        if not self._is_initialized:
            self._initialize()
        if colorid is None:
            return ''
        for item in self.colors.iter('ITEM'):
            try:
                c = item.find('COLOR')  # tag with color number
                if c is not None and c.text is not None:
                    if colorid == int(c.text):
                        crgb = item.find('COLORRGB')
                        if crgb is not None and crgb.text is not None:
                            return str(crgb.text)
            except ValueError:
                pass
        return 'Unknown color'

    def part_name(self, partid: Optional[str]) -> str:
        """Return part name defined by part ID."""
        # input: str with brick part identification number
        # output: string with brink name based on partid value
        if not self._is_initialized:
            self._initialize()
        for item in self.parts.iter('ITEM'):
            pid = item.find('ITEMID')
            if pid is not None and pid.text is not None:
                if partid == pid.text.strip():
                    pidn = item.find('ITEMNAME')
                    if pidn is not None and pidn.text is not None:
                        return pidn.text
        return 'Unknown part name'

    def item_type_values(self) -> List[str]:
        """Returns list of only possible values of item types."""
        return self._item_type_values


class BLitem:
    """Class BL item (lot) from BL wanted list or order list."""
    def __init__(self, item: ET.Element) -> None:
        """Makes BL item from XML element."""
        # this is the ET.Element containing data:
        self.item = item
        # Class cannot carry any local variables, because e.g.
        # BLlist.allitems() creates new BLitem instances (finds items using
        # ET.findall('ITEM'))

    @property
    def type(self) -> str:
        """Returns BL item type (set/part/minifig/book/gear/catalog/manual)."""
        # type must be always present, and is string, so there is no reason to
        # have type_s property
        t = self.item.find('ITEMTYPE')
        if t is None or t.text is None:
            raise Exception('Missing type of an item. Incorrect list, '
                            'cannot continue!')
        return t.text

    @type.setter
    def type(self, tp: str) -> None:
        """Set BL item type (set/part/minifig/book/gear/catalog/manual).
        Required."""
        if tp is None:
            raise Exception('Type is required for item, it cannot be None!')
        # LegoCatalogue needed for testing correct value of item type:
        global LC
        if LC is None:
            LC = LegoCatalogue(_get_resource_dir())
        tp = tp.upper()
        e = self.item.find('ITEMTYPE')
        # check if item type is correct?
        if tp not in LC.item_type_values():
            # bad value of type, error!
            raise Exception('Incorrect value of BL item type! '
                            'Only values S,P,M,B,G,C,I,O allowed.')
        if e is None:
            # element does not exist, create new element
            e = ET.SubElement(self.item, 'ITEMTYPE')
        e.text = str(tp)

    @property
    def id(self) -> str:
        """Returns BL item id (brick id)."""
        # id must be always present, and is string, so there is no reason to
        # have id_s property
        i = self.item.find('ITEMID')
        if i is None or i.text is None:
            raise Exception('Missing id of an item. Incorrect list, '
                            'cannot continue!')
        return i.text

    @id.setter
    def id(self, id: str) -> None:
        """Set BL item id (brick id). Required."""
        e = self.item.find('ITEMID')
        if id is None:
            raise Exception('ID is required for item, it cannot be None!')
        if e is None:
            # element does not exist, create new element
            e = ET.SubElement(self.item, 'ITEMID')
        e.text = id

    @property
    def color(self) -> Optional[int]:
        """Return BL item color (brick color)."""
        c = self.item.find('COLOR')
        if c is None or c.text is None:
            return None
        else:
            return int(c.text)

    @color.setter
    def color(self, color: int) -> None:
        """Set BL item color (brick color)."""
        self._resolve_setter('COLOR', color)

    @property
    def color_s(self) -> str:
        """Return BL item color (brick color) as string."""
        c = self.color
        if c is None:
            return 'Not Applicable'
        else:
            return str(c)

    @property
    def maxprice(self) -> Optional[float]:
        """Return BL item maximum price."""
        p = self.item.find('MAXPRICE')
        if p is None or p.text is None:
            return None
        else:
            return float(p.text)

    @maxprice.setter
    def maxprice(self, maxprice: float) -> None:
        """Set BL item maximum price."""
        self._resolve_setter('MAXPRICE', maxprice)

    @property
    def maxprice_s(self) -> str:
        """Return BL item maximum price as string."""
        p = self.maxprice
        if p is None:
            return ''
        else:
            return str(p)

    @property
    def minqty(self) -> Optional[int]:
        """Return BL item minqty (wanted quantity)."""
        q = self.item.find('MINQTY')
        if q is None or q.text is None:
            return None
        else:
            return int(q.text)

    @minqty.setter
    def minqty(self, minqty: int) -> None:
        """Set BL item minqty (wanted quantity)."""
        minqty = max(0, minqty) if minqty is not None else None
        self._resolve_setter('MINQTY', minqty)

    @property
    def minqty_s(self) -> str:
        """Return BL item minqty (wanted quantity) as string."""
        q = self.minqty
        if q is None:
            return '0'
        else:
            return str(q)

    @property
    def qtyfilled(self) -> Optional[int]:
        """Return BL item qtyfilled (already have quantity)."""
        q = self.item.find('QTYFILLED')
        if q is None or q.text is None:
            return None
        else:
            return int(q.text)

    @qtyfilled.setter
    def qtyfilled(self, qtyfilled: int) -> None:
        """Set BL item qtyfilled (already have quantity)."""
        qtyfilled = max(0, qtyfilled) if qtyfilled is not None else None
        self._resolve_setter('QTYFILLED', qtyfilled)

    @property
    def qtyfilled_s(self) -> str:
        """Return BL item qtyfilled (already have quantity) as string."""
        q = self.qtyfilled
        if q is None:
            return '0'
        else:
            return str(q)

    @property
    def condition(self) -> Optional[str]:
        """Return BL item condition (new or used)."""
        c = self.item.find('CONDITION')
        if c is None:
            return None
        else:
            return c.text

    @condition.setter
    def condition(self, condition: str) -> None:
        """Set BL item condition (new or used)."""
        # according the Bricklink help, the condition can be only 'U' or 'N'.
        # However in exports from Bricklink, the condition is also 'X'. Because
        # of this here will be no check of correct value.
        self._resolve_setter('CONDITION', condition)

    @property
    def condition_s(self) -> str:
        """Return BL item condition (new or used) as string."""
        c = self.condition
        if c is None:
            return ''
        else:
            if c.upper() == 'U':
                return 'Used'
            elif c.upper() == 'N':
                return 'New'
            else:
                return c

    @property
    def remarks(self) -> Optional[str]:
        """Return BL item remarks (comment)."""
        # conversion to/from html?
        r = self.item.find('REMARKS')
        if r is None:
            return None
        else:
            return r.text

    @remarks.setter
    def remarks(self, remarks: str) -> None:
        """Set BL item remarks (comment)."""
        self._resolve_setter('REMARKS', remarks)

    @property
    def remarks_s(self) -> str:
        """Return BL item remarks (comment) as string."""
        r = self.remarks
        if r is None:
            return ''
        else:
            return r

    @property
    def notify(self) -> Optional[str]:
        """Return BL item notify (notification by email)."""
        n = self.item.find('NOTIFY')
        if n is None:
            return None
        else:
            return n.text

    @notify.setter
    def notify(self, notify: str) -> None:
        """Set BL item notify (notification by email)."""
        e = self.item.find('NOTIFY')
        if notify is None:  # cases 1,2
            if e is not None:
                self.item.remove(e)  # case 2
            # otherwise resolves cases 1
        else:
            if e is None:  # case 3
                # element does not exist, create new element
                e = ET.SubElement(self.item, 'NOTIFY')
            e.text = str(notify)  # case (3), 4

    @property
    def notify_s(self) -> str:
        """Return BL item notify (notification by email) as string."""
        n = self.notify
        if n is None:
            return ''
        else:
            return n

    @property
    def wantedshow(self) -> Optional[str]:
        """Return BL item for sale."""
        w = self.item.find('WANTEDSHOW')
        if w is None:
            return None
        else:
            return w.text

    @wantedshow.setter
    def wantedshow(self, wantedshow: str) -> None:
        """Set BL item for sale."""
        self._resolve_setter('WANTEDSHOW', wantedshow)

    @property
    def wantedshow_s(self) -> str:
        """Return BL item for sale as string."""
        w = self.wantedshow
        if w is None:
            return ''
        else:
            return w

    @property
    def wantedid(self) -> Optional[int]:
        """Return BL item wanted list id."""
        i = self.item.find('WANTEDLISTID')
        if i is None or i.text is None:
            return None
        else:
            return int(i.text)

    @wantedid.setter
    def wantedid(self, wantedid: int) -> None:
        """Set BL item wanted list id."""
        self._resolve_setter('WANTEDLISTID', wantedid)

    @property
    def wantedid_s(self) -> Optional[str]:
        """Return BL item wanted list id as str."""
        i = self.wantedid
        if i is None:
            return ''
        else:
            return str(i)

    @property
    def missing(self) -> Optional[int]:
        """Return BL item missing quantity. None if minqty (wanted) is None. If
        qtyfilled is None, suppose it is 0."""
        m = self.minqty     # wanted
        q = self.qtyfilled  # have
        # if wanted is None, missing cannot be calculated
        if m is None:
            return None
        # if have is None, set it to 0:
        if q is None:
            q = 0
        missing = m - q
        # missing can be only zero or positive value. negative would mean
        # surplus
        if missing < 0:
            missing = 0
        return missing

    # property missing does not have got a setter, because value of missing is
    # given by values of minqty (wanted) and qtyfilled (have).

    @property
    def missing_s(self) -> Optional[str]:
        """Return BL item missing qty as string."""
        m = self.missing
        if m is None:
            return ''
        else:
            return str(m)

    def add_item(self, B: 'BLitem') -> None:
        """Adds bricks from other BLitem to self. Other BLitem is untouched."""
        # check if it is possible to add bricks:
        if not same_brick(self, B):
            raise Exception('Cannot add bricks with different '
                            'type, id or color!')
        # set values of self based on self and B:
        self.maxprice = self._minfloat(self.maxprice, B.maxprice)
        self.minqty = self._sum(self.minqty, B.minqty)
        self.qtyfilled = self._sum(self.qtyfilled, B.qtyfilled)
        self.remarks = self._join(self.remarks, B.remarks)
        self.notify = self._yesnotify(self.notify, B.notify)
        # joins string values #XXX is it correct?
        self.wantedshow = self._join(self.wantedshow, B.wantedshow)
        # keeps value from self #XXX is it correct?
        self.wantedid = self._minint(self.wantedid, B.wantedid)

    def _minfloat(self,
                  float1: Optional[float],
                  float2: Optional[float]) -> Optional[float]:
        """Returns lower of values. Handles None values."""
        # Used for adding maxprices of two items.
        if float1 is not None and float2 is not None:
            return min(float1, float2)
        elif float1 is not None and float2 is None:
            return float1
        elif float1 is None and float2 is not None:
            return float2
        else:
            return None

    def _sum(self, val1: Optional[int], val2: Optional[int]) -> Optional[int]:
        """Sums values. Handles None values."""
        # Used for adding minqty and qtyfilled of two items.
        if val1 is not None and val2 is not None:
            return val1 + val2
        elif val1 is not None and val2 is None:
            return val1
        elif val1 is None and val2 is not None:
            return val2
        else:
            return None

    def _newer(self, condition1: Optional[str],
               condition2: Optional[str]) -> Optional[str]:
        """Returns newer of two conditions. Prefer 'N', or 'U', or first value.
        Handles None values."""
        # Used for adding condition of two items.
        if condition1 is not None and condition2 is not None:
            if condition1.upper() == 'N' or condition2.upper() == 'N':
                return 'N'
            elif condition1.upper() == 'U' or condition2.upper() == 'U':
                return 'U'
            else:
                return condition1.upper()
        elif condition1 is not None and condition2 is None:
            return condition1
        elif condition1 is None and condition2 is not None:
            return condition2
        else:
            return None

    def _join(self, str1: Optional[str], str2: Optional[str]) -> Optional[str]:
        """Sums strings. Handles None values."""
        # Used for adding remarks together
        if str1 is not None and str2 is not None:
            return str1 + ', ' + str2
        elif str1 is not None and str2 is None:
            return str1
        elif str1 is None and str2 is not None:
            return str2
        else:
            return None

    def _yesnotify(self,
                   notify1: Optional[str],
                   notify2: Optional[str]) -> Optional[str]:
        """Returns Y of two notifies. Prefer 'Y', or 'N', or first value.
        Handles None values."""
        # Used for adding notify of two items.
        if notify1 is not None and notify2 is not None:
            if notify1.upper() == 'Y' or notify2.upper() == 'Y':
                return 'Y'
            elif notify1.upper() == 'N' or notify2.upper() == 'N':
                return 'N'
            else:
                return notify1.upper()
        elif notify1 is not None and notify2 is None:
            return notify1
        elif notify1 is None and notify2 is not None:
            return notify2
        else:
            return None

    def _minint(self,
                int1: Optional[int],
                int2: Optional[int]) -> Optional[int]:
        """Returns lower of two integers. Handles None values."""
        # Used for selecting wantedid of two items.
        if int1 is not None and int2 is not None:
            return min(int1, int2)
        elif int1 is not None and int2 is None:
            return int1
        elif int1 is None and int2 is not None:
            return int2
        else:
            return None

    def _resolve_setter(self, parameter: str, new: Union[float, int, str]):
        """Code for setter of BLitem parameters, because it is very similar
        for most setters. Sets values and correctly handles None inputs."""
        # boolean matrix:
        # case No | new value | actual | action
        # --------|-----------|--------|-----------------
        #      1  | None      | None   | no action required
        #      2  | None      | exist  | delete element actual
        #      3  | exist     | None   | create new element, set value
        #      4  | exist     | exist  | set value to max(0, new)

        # get actual value of the parameter:
        actual = self.item.find(parameter)
        if new is None:
            if actual is not None:
                # case 2: delete element actual
                self.item.remove(actual)
            # case 1:  nothing to do
        else:
            if actual is None:
                # case 3, element does not exist, create new element:
                actual = ET.SubElement(self.item, parameter)
                # case 4 and 3: set non-negative value:
            actual.text = str(new)  # case (3),4

    def _libdata_dump(self, data: Dict) -> None:
        """Store library private data into xml of BLitem, tag LIBDATA, for
        later use. This tag will be deleted when saving into xml file."""
        el = self.item.find('LIBDATA')
        if el is None:
            # tag doesn't exist, create it and store new data:
                el = ET.SubElement(self.item, 'LIBDATA')
        el.text = json.dumps(data)

    def _libdata_load(self) -> Dict:
        """Load library private data into xml of BLitem, tag LIBDATA, for later
        use. This tag will be deleted when saving into xml file."""
        out: Dict[str, List[List[Union[str, int]]]] = dict()
        el = self.item.find('LIBDATA')
        if el is not None:
            if el.text is not None:
                out = json.loads(el.text)
        return out

    def _libdata_purge(self):
        """Remove library private data from xml of BLitem, tag LIBDATA."""
        el = self.item.find('LIBDATA')
        if el is not None:
            # remove the element
            self.item.remove(el)

    def _libdata_add_mablor(self, isW: bool, name: str, ammount: int, color: int) -> None:
        """Adds notes from (M)ABLOR processing. These notes will be formatted
        and added to remarks during writing the XML/HTML. isW: for wanted
        lists, name: source file of bricks, ammount: number of transferred
        bricks."""
        # library private data scheme:
        # dictionary with keys 'mablorW', 'mablorO', 'summ'
        # each key contain array of arrays
        # for mablorW: [[name, ammount, color], ...]
        # for mablorO: [[name, ammount, color], ...]
        # (where name is name of file, color is brick color, ammount is amount
        # of transfered bricks)
        # for summ:    [[name, haveammount, wantedammount, color], ...]
        # (where haveammount and wantedammount are qtyfilled and minqty)
        #
        # load library private data:
        libdata = self._libdata_load()
        if isW:
            # handle case of wanted list:
            if 'mablorW' not in libdata:
                # not yet any library private data
                mablorW: List[List[Union[str, int]]] = []
            else:
                mablorW = libdata['mablorW']
            mablorW.append([name, ammount, color])
            libdata['mablorW'] = mablorW
        else:
            # handle case of order list:
            if 'mablorO' not in libdata:
                # not yet any library private data
                mablorO: List[List[Union[str, int]]] = []
            else:
                mablorO = libdata['mablorO']
            mablorO.append([name, ammount, color])
            libdata['mablorO'] = mablorO
        # save modified library data
        self._libdata_dump(libdata)

    def _libdata_mablor_format(self):
        """Formats information about transferred bricks into string suitable
        for remarks of an BL item."""
        # load library private data
        libdata = self._libdata_load()
        if 'mablorW' not in libdata:
            mablorW: List[List[Union[str, int]]] = []
        else:
            mablorW = libdata['mablorW']
        if 'mablorO' not in libdata:
            mablorO: List[List[Union[str, int]]] = []
        else:
            mablorO = libdata['mablorO']
        # import lego catalogue
        global LC
        if LC is None:
            LC = LegoCatalogue(_get_resource_dir())
        # make the string
        s = ''
        # data for wanted and order lists:
        for l in (mablorW, mablorO):
            for t in l:
                if t[1] != 0:
                    # only if not zero value transfer
                    color = (_svg_tag(t[2], 10) +
                             '&nbsp' +
                             LC.color_name(t[2])
                             )
                    s = s + '<BR><NOBR><span style="color:green;">' + \
                        '{}:H+{}({})'.format(t[0], t[1], color) + \
                        '</span></NOBR>'
        return s

    def _libdata_add_summ(self,
                          name,
                          haveammount,
                          wantedammount,
                          color) -> None:
        """Adds notes from adding two lists. These notes will be formatted
        and added to remarks during writing the XML/HTML. Name: source file of
        bricks."""
        # library private data scheme:
        # dictionary with keys 'mablorW', 'mablorO', 'summ'
        # each key contain array of arrays
        # for mablorW: [[name, ammount, color], ...]
        # for mablorO: [[name, ammount, color], ...]
        # (where name is name of file, color is brick color, ammount is amount
        # of transfered bricks)
        # for summ:    [[name, haveammount, wantedammount, color], ...]
        # (where haveammount and wantedammount are qtyfilled and minqty)
        #
        # load library private data
        libdata = self._libdata_load()
        if 'summ' not in libdata:
            summ: List[List[Union[str, int]]] = []
        else:
            summ = libdata['summ']
        # add new data
        summ.append([name, wantedammount, haveammount, color])
        libdata['summ'] = summ
        # save library private data
        self._libdata_dump(libdata)

    def _libdata_summ_format(self):
        """Formats information about summed bricks into string suitable for
        remarks of an BL item."""
        # load library private data
        libdata = self._libdata_load()
        if 'summ' not in libdata:
            summ = []
        else:
            summ = libdata['summ']
        s = ''
        # data for wanted and order lists:
        for t in summ:
            if t[1] != 0:
                # only if not zero value transfer
                t[1] = 0 if t[1] is None else t[1]
                t[2] = 0 if t[2] is None else t[2]
                s = s + '<BR><NOBR><span style="color:green;">' + \
                    '{}:W+{},H+{}'.format(t[0], t[1], t[2]) + \
                    '</span></NOBR>'
        return s


class BLlist:
    """Class: BL wanted list or order list."""
    def __init__(self, filename: str, name: Optional[str]=None) -> None:
        """Parse BL wanted or order xml file. Name is optional name of the list
        that will be used in remarks, logs etc. If name not set, file name will
        be used."""
        self.ET = ET.parse(filename)
        # assign value to the name - name of list (used in remarks, logs etc.)
        if name is not None:
            self.listname = name
        else:
            # name was not set by user, filename without extension will serve
            # as list name
            fn, ext = os.path.splitext(os.path.basename(filename))
            self.listname = fn

    def __add__(self, other: 'BLlist') -> 'BLlist':
        """Returns new BLlist as a result of merging two BLlists."""
        # create new resulting list from first list:
        newList = copy.deepcopy(self)
        # create new list from second list so it is not changed by following
        # operations:
        otherList = copy.deepcopy(other)
        # go through all items in second list and merge or add it to new list
        for otherListItem in otherList.all_items():
            itemfound = False
            # try to find matching item in new list
            for newListItem in newList.all_items():
                if same_brick(otherListItem, newListItem):
                    # same item found, merge bricks
                    newListItem.add_item(otherListItem)
                    #  add library private data with information how much from
                    #  other list
                    newListItem._libdata_add_summ(otherList.name,
                                                  otherListItem.qtyfilled,
                                                  otherListItem.minqty,
                                                  otherListItem.color)
                    itemfound = True
                    break
            # if matching item was not found, create new item in new list:
            if itemfound is False:
                # add item(lot) from other List to the new list:
                addedItem = newList.add_item(otherListItem)
                #  add library private data with information how much from
                #  other list
                addedItem._libdata_add_summ(otherList.name,
                                            otherListItem.qtyfilled,
                                            otherListItem.minqty,
                                            otherListItem.color)
        return newList

    @property
    def name(self) -> str:
        """Returns name of the list."""
        return self.listname

    @name.setter
    def name(self, name: str) -> None:
        """Set name of the list."""
        self.listname = name

    def copy(self) -> 'BLlist':
        """ Returns new copy of the BLlist."""
        return copy.deepcopy(self)

    def all_items(self) -> List[BLitem]:
        """Returns all BL items as BL list."""
        items = self.ET.findall('ITEM')
        BLitems = []
        for item in items:
            BLitems.append(BLitem(item))
        return BLitems

    def html(self) -> str:
        """Converts BL list into html webpage."""
        # initialize lego catalogue if needed:
        global LC
        if LC is None:
            LC = LegoCatalogue(_get_resource_dir())
        # Table sorting functions are copied (and modified) from project:
        # https://github.com/853419196/IntlTableSort
        table = []
        # for all items
        for row, I in enumerate(self.all_items()):
            # make row from item
            # insert brick name and link to brick image of correct color:
            name = (LC.part_name(I.id) +
                    '<br>' +
                    _get_item_img_link(I.id, I.color)
                    )
            # insert color name and box:
            color = (LC.color_name(I.color) +
                     '<br>' +
                     _svg_tag(I.color)
                     )
            # something can be None, so _s calls have to be used.
            # prepare remarks string:
            rs = I.remarks_s + \
                I._libdata_mablor_format() + \
                I._libdata_summ_format()
            table.append([name,
                          color,
                          I.missing_s,
                          I.type,
                          I.id,
                          I.color_s,
                          I.maxprice_s,
                          I.minqty_s,
                          I.qtyfilled_s,
                          I.condition_s,
                          rs,
                          I.notify_s,
                          I.wantedshow_s,
                          I.wantedid_s
                          ])
        # create html table from list of lists:
        tablestr = _list_to_html_table(table)
        # load templates:
        templates = _get_list_html_templates()
        # concatenate into complete html document:
        doc = (templates[0] +
               '\n' +
               _html_title(self.name) +
               '\n' +
               self._status_description() +
               '\n' +
               templates[1] +
               '\n' +
               tablestr +
               '\n' +
               templates[2])
        # return html webpage
        return doc

    def html_save(self, filename: str) -> None:
        """Converts BL list into html and save it into a file."""
        doc = self.html()
        f = open(filename, 'w', encoding='utf8')
        f.write(doc)
        f.close()

    def xml_save(self, filename: str) -> None:
        """Save BL list into xml file."""
        # cleans library private data if found:
        # make a copy of itself so libdata can be purged and deep copied xml
        # saved, and still keep libdata for next processing:
        newList = copy.deepcopy(self)
        for el in newList.all_items():
            el._libdata_purge()
        # this version produced output with long single line in case of
        # create_item:
        newList.name = filename
        R = newList.ET.getroot()
        newList._indent(R)
        newList.ET.write(filename, encoding='UTF-8', xml_declaration=True)

    def rem_full(self) -> None:
        """Removes all items (lots) that are full.

        Removes all items (lots) from BLlist that are full, this means that for
        particular item (lot) to be removed from the list, qtyfilled (have)
        quantity must be equal or greater than minqty(wanted)."""
        for I in self.all_items():
            if I.minqty is not None and I.qtyfilled is not None:
                if I.qtyfilled >= I.minqty:
                    self.rem_item(I)

    def rem_item(self, item: BLitem) -> None:
        """Removes item (lot) from BLlist."""
        R = self.ET.getroot()
        R.remove(item.item)

    def create_item(self, itemtype: str, itemid: str) -> BLitem:
        """Creates new item (lot) based on item type and item id."""
        # gets root, create element item and fill in itemtype and itemid. Other
        # are not required and can be set by setters.
        R = self.ET.getroot()
        newItem = BLitem(ET.SubElement(R, 'ITEM'))
        newItem.type = itemtype
        newItem.id = itemid
        return newItem

    def add_item(self, item: BLitem) -> BLitem:
        """Copy other item (lot) into the list."""
        newI = self.create_item(item.type, item.id)
        newI.color = item.color
        newI.maxprice = item.maxprice
        newI.minqty = item.minqty
        newI.qtyfilled = item.qtyfilled
        newI.condition = item.condition
        newI.remarks = item.remarks
        newI.notify = item.notify
        newI.wantedshow = item.wantedshow
        newI.wantedid = item.wantedid
        newI.qtyfilled = item.qtyfilled
        return newI

    def status(self) -> List[int]:
        """Returns status of list with number of:
            [have bricks, bricks, filled lots, lots]."""
        havebricks = 0  # number of have bricks in whole list
        wantedbricks = 0  # number of all wanted bricks
        havelots = 0  # number of lots with have >= wanted
        wantedlots = 0  # number of lots
        # through all lots count bricks and lots:
        for I in self.all_items():
            wantedlots = wantedlots + 1
            qtyfilled = I.qtyfilled
            minqty = I.minqty
            if qtyfilled is None:
                qtyfilled = 0
            if minqty is None:
                minqty = 0
            havebricks += qtyfilled
            wantedbricks += minqty
            # if have >= wanted, lot is filled
            if qtyfilled >= minqty:
                havelots = havelots + 1
        return [havebricks, wantedbricks, havelots, wantedlots]

    def _status_description(self) -> str:
        """Makes nice formated status string."""
        [havebricks, bricks, havelots, lots] = self.status()
        # calculate percentage of filled bricks, care about zero dividing:
        bricks_pct = 0
        if bricks != 0:
            bricks_pct = round(havebricks/bricks*100)
        # calculate percentage of filled lots, care about zero dividing:
        lots_pct = 0
        if lots != 0:
            lots_pct = round(havelots/lots*100)
        s = 'List contains ' + \
            str(havebricks) + \
            ' brick(s) of ' + \
            str(bricks) + \
            ' wanted (' + \
            str(bricks_pct) + \
            ' %) and ' + \
            str(havelots) + \
            ' filled lot(s) of ' + \
            str(lots) + \
            ' lots (' + \
            str(lots_pct) + \
            ' %).'
        return s

    def _indent(self,
                elem: 'xml.etree.ElementTree.Element',
                level: int=0) -> None:
        """Ensure proper new lines in xml. Use before saving to xml file."""
        # level is partially disabled, because it is not used in BL xml files
        # based on:
        # https://stackoverflow.com/questions/3095434/inserting-newlines-in-xml-file-generated-via-xml-etree-elementtree-in-python
        #  original line: i = "\n" + level*"  "
        i = "\n"
        if len(elem):
            if not elem.text or not elem.text.strip():
                # original line:  elem.text = i + "  "
                elem.text = i + ""
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                # original line:  indent(elem, level+1)
                self._indent(elem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

    def libdata_purge(self) -> None:
        """Remove library private data from xml of all BLitems, tag LIBDATA.
        Usefull to get senseful remarks in html output."""
        BLitems = self.all_items()
        for item in BLitems:
            item._libdata_purge


# XXX dodelat? je to treba?
#  def same_items(A: BLitem, B: BLitem) -> bool:
#      """Returns if both BL items are of equal type, id, color,
#         condition, qtyfilled, minqty (wanted)."""
#      raise error 2DO
#
#  def same_lot(A: BLitem, B: BLitem) -> bool:
#      """Returns if both BL items are of equal type, id, color,
#         condition."""
#      raise error 2DO
#

def summ(BLlists: List[BLlist]) -> BLlist:
    """Makes sum of BL lists and return a new BL list. Sets remarks
    properly."""
    # copy first list into new one:
    sumlist = BLlists[0].copy()
    # set libdata of all items in the first list, because summing in for cycle
    # goes from 1 and not from 0
    for item in sumlist.all_items():
        item._libdata_add_summ(sumlist.name,
                               item.qtyfilled,
                               item.minqty,
                               item.color)
    # add rest of lists
    for i in range(1, len(BLlists)):
        sumlist = sumlist + BLlists[i]
    return sumlist


def ABLOR(O: BLlist, W: BLlist) -> None:
    """Apply order to wanted list. Wanted list will have increased have count,
    and order list will have decreased ordered count."""
    # iterate through all wanted list items
    # find if item is in order
    # add maximum possible quantity to wanted list and remove from order
    # reorder items so items with color any are last ones;
    Witems = _reorder_for_ablor(W)
    # go through all ordered wanted list items and make transfers:
    for Witem in Witems:
        for Oitem in O.all_items():
            # XXX multiple items have to be found! but there is no
            # possibility to have more compatible items in order! but
            # what if this happens?
            if transfer_compatible(Oitem, Witem):
                # brick found and transfer possible. amount of bricks to
                # transfer will be determined automatically:
                amount = transfer_bricks(Oitem, Witem)
                Witem._libdata_add_mablor(True, O.name, amount, Oitem.color)
                Oitem._libdata_add_mablor(False, W.name, amount, Witem.color)
            else:
                pass
                #  XXX what if not compatible? should be! error in Order?

    # array with results for log and summary message:
    # collumn order:
    # id, color, WL wants, WL have, O have, action, WL added,
    # WL new have, O new have, missing


def MABLOR(Olist: List[BLlist], Wlist: List[BLlist]) -> None:
    """Applies ABLOR to multiple wanted lists and multiple orders. The order is
    important."""
    for W in Wlist:
        for O in Olist:
            ABLOR(O, W)


def same_type_id(A: BLitem, B: BLitem) -> bool:
    """Returns if both BL items are of equal type, id."""
    if A.type.strip().upper() != B.type.strip().upper():
        return False
    if A.id.strip().upper() != B.id.strip().upper():
        return False
    return True


def same_brick(A: BLitem, B: BLitem) -> bool:
    """Returns if both BL items are of equal type, id, color."""
    if same_type_id(A, B) is False:
        return False
    if A.color != B.color:
        return False
    return True


def transfer_compatible(A: BLitem, B: BLitem) -> bool:
    """Checks if transfer of bricks for ABLOR is possible from A to B. Returns
    if both items are of equal type and id. Color must be either same or B must
    be of None color (any). Minqty (have) of A must be a number."""
    if same_type_id(A, B) is False:
        return False
    # now items are of same type and same id.
    if B.color is not None:
        if A.color != B.color:
            return False
    # now B.color is none or A.color == B.color
    if A.minqty is None:
        return False
    # now A has integer have quantity
    # all conditions are fulfilled:
    return True


def transfer_bricks(A: 'BLitem',
                    B: 'BLitem',
                    amount: Optional[int]=None) -> int:
    """Transfer amount of bricks from A to B. Minqty of bricks in A are
    decreased, and qtyfilled of bricks in B is increased. If amount is not set
    or None, as many as possible bricks are transferred to match minqty
    (wanted) quantity. Function suppose bricks are transfer compatible (see
    function transfer_compatible)."""
    # transfer direction: A.minqty->B.qtyfilled
    # check if minimum of information was delivered by user:
    if B.minqty is None and amount is None:
        # no way how to automatically find out transfer amount if
        # self.minqty and amount are none
        raise Exception('Do not know how many bricks should be transfered '
                        'because amount is None and B.minqty (wanted) is '
                        'also None.')
    if amount is None:
        # automatic determination of amount to transfer. Take smaller value
        # of available bricks and missing bricks. A.minqty should be number
        # because it should have been check by transfer_compatible. B.missing
        # is not None because amount is None therefore B.minqty is not None so
        # B.missing must be actual value.
            amount = min([A.minqty, B.missing])
    else:
        # amount is actual number, A.minqty is checked by transfer_compatbile
        if amount > A.minqty:
            # required too many bricks to transfer
            raise Exception('Not enough bricks to transfer!')
    # finally make the actual transfer:
    if B.qtyfilled is None:
        # suppose B.qtyfilled is zero
        B.qtyfilled = amount
    else:
        B.qtyfilled = B.qtyfilled + amount
    A.minqty = A.minqty - amount
    return amount
    # NOTE XXX TO REMARK HOW MUCH FROM WHERE!


def _reorder_for_ablor(L: BLlist) -> List[BLitem]:
    """Returns list of BLitems ordered such way items with color None (any) are
    last. Returns only items with nonzero missing quantity."""
    # This is important for ABLOR because first items from order has to be
    # transferred to wanted items with specific color, and after this rest of
    # items will be transferred to wanted items with color any.
    first = []
    last = []
    for item in L.all_items():
        if item.missing is not None and item.missing > 0:
            if item.color is not None:
                # color is not none, add item to first:
                first.append(item)
            else:
                print('Item (' + item.id + '/' + item.color_s +
                      ') will be evaluated last.')
                # color is none, add item to last:
                last.append(item)
    # join the two lists:
    first.extend(last)
    return first


def _get_resource_dir() -> str:
    """Returns directory with resources."""
    # Check if it runs from PyInstaller bundle or Python interpreter:
    # (_MEIPASS cause error in mypy, however the code is correct)
    if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
        # it is bundle
        return os.path.join(sys._MEIPASS, 'resources')
    else:
        # it is python interpreter
        return os.path.join(os.path.dirname(os.path.realpath(__file__)),
                            'resources')


def _get_list_html_templates() -> List[str]:
        """Load parts of list template."""
        templates = []
        # get directory of templates:
        rd = _get_resource_dir()
        # read templates, names of templates in constant:
        for fn in LIST_TEMPLATE_FILENAMES:
            fn = os.path.join(rd, fn)
            f = open(fn, 'r', encoding='utf8')
            templates.append(f.read())
            f.close()
        return templates


def _svg_tag(color: Optional[int], boxsize: int=50) -> str:
    """Create SVG image of color box based on color id with set size of box."""
    # initialize lego catalogue if needed:
    global LC
    if LC is None:
        LC = LegoCatalogue(_get_resource_dir())
    if color is None:
        # color is None, not specified, so color box will contain rainbow
        clr = ' fill="url(#r)"'
        # make definition of color gradient:
        defs = """
               <defs><linearGradient id="r">
               <stop offset="0" stop-color="red"/>
               <stop offset="0.333" stop-color="#ff0"/>
               <stop offset="0.5" stop-color="#0f0"/>
               <stop offset="0.666" stop-color="cyan"/>
               <stop offset="0.833" stop-color="blue"/>
               <stop offset="1" stop-color="#f0f"/>
               </linearGradient></defs>
               """
    else:
        # get rgb value of color:
        clr = ('fill="#' +
               LC.color_hex(color) +
               '"')
        defs = ''
    # join all together
    s = ('<svg width="' +
         str(boxsize) +
         '" height="' +
         str(boxsize) +
         '">' +
         defs +
         '<rect width="' +
         str(boxsize) +
         '" height="' +
         str(boxsize) +
         '" ' +
         clr +
         ' /></svg>')
    return s


def _list_to_html_table(data: List[List[Optional[str]]]) -> str:
    """Convert list of lists of strings to html table body."""
    # table start and table header is already part of html template
    q = "<tbody>"
    for row in data:
        q += "\n<tr>"
        for el in row:
            if el is None:
                es = ''
            else:
                es = el
            q += "\n    <td>" + es + "</td>"
        q += "\n</tr>"
    # finish table:
    q += "\n</tbody>\n</table>"
    return q


def _get_item_img_link(itemid: str, color: Optional[int]) -> str:
    """Generate html link to a brick image from Bricklink web."""
    if color is None:
        # color cannot be selected, so make link to a brick 3D image at
        # bricklink that is in general gray color
        # link template:
        #   https://www.bricklink.com/3D/images/3001/640/01.png
        l = ('<IMG SRC="https://www.bricklink.com/3D/images/' +
             str(itemid) +
             '/' + '640/01.png">'
             )
        return l
    else:
        # make link to an image of brick of proper color at bricklink
        # link template:
        #   https://img.bricklink.com/ItemImage/PN/7/3001.png
        l = ('<IMG SRC="https://img.bricklink.com/ItemImage/PN/' +
             str(color) +
             '/' +
             str(itemid) +
             '.png">'
             )
        return l


def _html_title(title: str) -> str:
    """Create title for html page."""
    return ('<H3>' + title + '</H3>')


# XXX 2DO:
# subtract two lists
