#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# contains functions and html parts for generation of html log
# table sorting functions are copied (and modified) from project:
# https://github.com/853419196/IntlTableSort

# collumns in final log:
#  ID
#  Color ID
#  Color - not in source table, added here
#  Name - not in source table, added here
#  Wants
#  Have
#  Action
#  In order
#  Added to WL
#  Left in order
#  New have
#  WL still missing

import legodata
import re
import os
import sys

# global with catalogue data, thus reading of xml files is done only once
cat = None


def make_log_html(res, cmd, title):
    global cat
    # main function
    # generate html document with result

    # get script location so resources are found even if called from other
    # directory:
    if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
        # running in a PyInstaller bundle
        resdr = os.path.join(sys._MEIPASS, 'resources')
    else:
        # running from python interpreter
        resdr = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                          'resources')
    cat = legodata.catalogue(resdr)

    # add and format columns in result:
    for i, row in enumerate(res):
        # insert brick name and link to brick image of correct color:
        row.insert(2, (cat.part_name(row[0]) +
                       '<br>' +
                       getitemimglink(row[0], row[1])))
        # insert color name and box:
        row.insert(2, cat.color_name(row[1]) + '<br>' + svg_tag(row[1]))
        #  format action to add color boxes
        if row[6].find('color') != -1:
            row[6] = replace_color_in_action(row[6])
    #  # create html table from list:
    tbl = list2htmltable(res)

    # load parts of template
    # template filenames:
    files = ['log-template-01.html',
             'log-template-02.html',
             'log-template-03.html']
    template = []
    # read templates:
    for fn in files:
        fn = os.path.join(resdr, fn)
        f = open(fn, 'r', encoding='utf8')
        template += [f.read()]
        f.close()

    # make whole html document
    doc = (template[0] +
           '\n' +
           titleandcmd(title, cmd) +
           '\n' +
           template[1] +
           '\n' +
           tbl +
           '\n' +
           template[2])

    return doc


def replace_color_in_action(s):
    # replace color id with color name in "action" collumn in html log
    global cat
    reg = re.compile('color (\d+)')
    f = ''
    for i, split in enumerate(reg.split(s)):
        if (i % 2):
            f += ('color <i>' +
                  cat.color_name(split) +
                  ' (' + split + ')' +
                  svg_tag(split, 15) +
                  '</i>'
                  )
        else:
            f += split
    return f


def svg_tag(color_id, boxsize=50):
    # create svg image as color box based on color id
    # if color id is 'any', color box contain rainbow
    global cat
    if color_id == 'any':
        clr = ' fill="url(#r)"'
        defs = """
               <defs><linearGradient id="r">
               <stop offset="0" stop-color="red"/>
               <stop offset="0.333" stop-color="#ff0"/>
               <stop offset="0.5" stop-color="#0f0"/>
               <stop offset="0.666" stop-color="cyan"/>
               <stop offset="0.833" stop-color="blue"/>
               <stop offset="1" stop-color="#f0f"/>
               </linearGradient></defs>
               """
    else:
        clr = ('fill="#' +
               cat.color_hex(color_id) +
               '"')
        defs = ''
    s = ('<svg width="' +
         str(boxsize) +
         '" height="' +
         str(boxsize) +
         '">' +
         defs +
         '<rect width="' +
         str(boxsize) +
         '" height="' +
         str(boxsize) +
         '" ' +
         clr +
         ' /></svg>')
    return s


def list2htmltable(data):
    # convert list to html table body
    # table start and table header is already part of html template
    q = "<tbody>"
    for row in data:
        q += "\n<tr>"
        for el in row:
            q += "\n    <td>" + el + "</td>"
        q += "\n</tr>"
    # finish table:
    q += "\n</tbody>\n</table>"
    return q


def getitemimglink(itemid, color):
    # generate html link to a brick image
    # at bricklink web
    if color == 'any':
        color = str(1)
    l = ('<IMG SRC="https://img.bricklink.com/ItemImage/PN/' +
         color + '/' + itemid + '.png">')
    return l


def titleandcmd(title, command):
    # create title for html log and add bash command
    s = ('<H3>' +
         title +
         '</H3>\n<P>Result for command:</P><P>' +
         command +
         '</P>')
    return s
